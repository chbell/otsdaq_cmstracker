#include "otsdaq-cmstracker/OTUtilities/PowerSupplyData.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

using namespace ots;

PowerSupplyData::PowerSupplyData()
{
}

PowerSupplyData::~PowerSupplyData()
{
}

void PowerSupplyData::addChannel(std::string group, std::string channel)
{
    configuredChannelsMap_[group][channel] = ChannelStatus();
}

void PowerSupplyData::setChannelStatus(std::string group, std::string channel, float current, float voltage, bool status, time_t time)
{
    configuredChannelsMap_[group][channel].current = current;
    configuredChannelsMap_[group][channel].voltage = voltage;
    configuredChannelsMap_[group][channel].status = status;
    configuredChannelsMap_[group][channel].time = time;
}

PowerSupplyData::ChannelStatus PowerSupplyData::getChannelStatus(std::string group, std::string channel)
{
    return configuredChannelsMap_[group][channel];
}

std::string PowerSupplyData::convertToJson()
{
    json j;
    for (auto &group : configuredChannelsMap_)
    {
        for (auto &channel : group.second)
        {
            j[className_][group.first][channel.first]["A"] = channel.second.current;
            j[className_][group.first][channel.first]["V"] = channel.second.voltage;
            j[className_][group.first][channel.first]["S"] = channel.second.status;
            j[className_][group.first][channel.first]["T"] = channel.second.time;
        }
    }
    return j.dump();
}

std::map<std::string, std::map<std::string, PowerSupplyData::ChannelStatus>> PowerSupplyData::convertFromJson(std::string jsonin)
{
    configuredChannelsMap_.clear();
    json j1 = json::parse(jsonin);
    for (auto &group : j1.items())
    {
        for (auto &channel : group.value().items())
        {
            for (auto &channel1 : channel.value().items())
            {
                configuredChannelsMap_[group.key()][channel1.key()].current = j1[group.key()][channel.key()][channel1.key()]["A"];
                configuredChannelsMap_[group.key()][channel1.key()].voltage = j1[group.key()][channel.key()][channel1.key()]["V"];
                configuredChannelsMap_[group.key()][channel1.key()].status = j1[group.key()][channel.key()][channel1.key()]["S"];
                configuredChannelsMap_[group.key()][channel1.key()].time = j1[group.key()][channel.key()][channel1.key()]["T"];
            }
        }
    }
    return configuredChannelsMap_;
}