#ifndef __PowerSupplyData_H__
#define __PowerSupplyData_H__

#include <string>
#include <map>

namespace ots
{
  class PowerSupplyData
  {
  public:
    struct ChannelStatus
    {
      float current;
      float voltage;
      bool status;
      time_t time;
    };
    PowerSupplyData();
    virtual ~PowerSupplyData();
    void addChannel(std::string group, std::string channel);
    void setChannelStatus(std::string group, std::string channel, float current, float voltage, bool status, time_t time);
    ChannelStatus getChannelStatus(std::string group, std::string channel);
    std::string convertToJson();
    std::map<std::string, std::map<std::string, ChannelStatus>> convertFromJson(std::string json);

  private:
    std::map<std::string, std::map<std::string, ChannelStatus>> configuredChannelsMap_;
    const std::string className_ = "PowerSupplyData";
    void dataValue(std::string json, bool vector);
  };
} // namespace ots
#endif
