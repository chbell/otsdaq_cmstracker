#ifndef _ots_DQMMiddlewareHistosConsumer_h_
#define _ots_DQMMiddlewareHistosConsumer_h_

#include "otsdaq/DataManager/DQMHistosConsumerBase.h"
#include "otsdaq/Configurable/Configurable.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/Container.h"
#include <string>

class DQMHistogramBase;
class TDirectory;

namespace ots
{

class DQMMiddlewareHistosConsumer : public DQMHistosConsumerBase, public Configurable
{
public:
  DQMMiddlewareHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~DQMMiddlewareHistosConsumer(void);

	void startProcessingData(std::string runNumber) override;
	void stopProcessingData (void) override;
	void pauseProcessingData (void) override;
	void resumeProcessingData(void) override;
	void load(std::string fileName){;}

private:
	bool workLoopThread(toolbox::task::WorkLoop* workLoop) override;
	void fastRead(void);
	void CopyDir (TDirectory *source);
	void ResetDir(TDirectory *source);
	const std::string currentDateTime();
	
	//For fast read
	std::string*                               dataP_;
	std::map<std::string,std::string>*         headerP_;

	bool                                       saveFile_; //yes or no
	std::string                                filePath_;
	std::string                                radixFileName_;
	std::vector<DQMHistogramBase*>             fDQMHistogrammerVector;
	std::vector<char>                          fDataBuffer;
	std::string                                calibrationName_;
	std::string                                configurationDir_;
	std::string                                configurationName_;
	std::string                                configurationFilePath_;
	std::string                                moduleConfigurationFileName_;
	DetectorContainer                          fDetectorStructure;
	int                                        iterationCounter_;
	std::atomic<bool>                          histogramsToReset_ {false};
};
}

#endif
