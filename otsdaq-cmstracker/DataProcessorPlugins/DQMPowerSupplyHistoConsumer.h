#ifndef _ots_DQMPowerSupplyHistoConsumer_h_
#define _ots_DQMPowerSupplyHistoConsumer_h_

#include "otsdaq/DataManager/DQMHistosConsumerBase.h"
#include "otsdaq/Configurable/Configurable.h"
#include "otsdaq-cmstracker/OTUtilities/PowerSupplyData.h"
#include <string>
namespace ots
{
class PowerSupplyDQMHistos;

class DQMPowerSupplyHistoConsumer : public DQMHistosConsumerBase, public Configurable
{
public:
  DQMPowerSupplyHistoConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~DQMPowerSupplyHistoConsumer(void);

	void startProcessingData (std::string               runNumber) override;
	void stopProcessingData  (void                               ) override;
	void pauseProcessingData (void                               ) override;
	void resumeProcessingData(void                               ) override;
	void load                (std::string               fileName ){;}
	
private:
	bool workLoopThread      (toolbox::task::WorkLoop * workLoop )         ;
	void fastRead            (void                               )         ;
	void slowRead            (void                               )         ;
	
	//For fast read
	std::string*                       dataP_        ;
	std::map<std::string,std::string>* headerP_      ;

	bool                               saveFile_     ; //yes or no
 	std::string                        filePath_     ;
	std::string                        radixFileName_;
	PowerSupplyData                    thePowerSupplyData_;
	PowerSupplyDQMHistos*              dqmHistos_;
	
};
}

#endif