#include "otsdaq-cmstracker/DataProcessorPlugins/DQMPowerSupplyHistoConsumer.h"
#include "otsdaq-cmstracker/OTDQMHistos/PowerSupplyDQMHistos.h"
#include "otsdaq/Macros/MessageTools.h"
#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/ProcessorPluginMacros.h"
#include <chrono>
#include <thread>
#include <TDirectory.h>
#include <TFile.h>
#include <TH1F.h>
#include <TRandom.h>
#include <TCanvas.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

using namespace ots;

//========================================================================================================================
DQMPowerSupplyHistoConsumer::DQMPowerSupplyHistoConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: WorkLoop             (processorUID)
	, DQMHistosConsumerBase(supervisorApplicationUID, bufferUID, processorUID, LowConsumerPriority)
	, Configurable         (theXDAQContextConfigTree, configurationPath)
	, saveFile_            (theXDAQContextConfigTree.getNode(configurationPath).getNode("SaveFile").getValue<bool>())
	, filePath_            (theXDAQContextConfigTree.getNode(configurationPath).getNode("FilePath").getValue<std::string>())
	, radixFileName_       (theXDAQContextConfigTree.getNode(configurationPath).getNode("RadixFileName").getValue<std::string>())
	, dqmHistos_           (nullptr)
{
}

//========================================================================================================================
DQMPowerSupplyHistoConsumer::~DQMPowerSupplyHistoConsumer(void)
{
	DQMHistosBase::closeFile();
	if(dqmHistos_ != nullptr)
	{
		delete dqmHistos_;
		dqmHistos_ = nullptr;
	}
}
//========================================================================================================================
void DQMPowerSupplyHistoConsumer::startProcessingData(std::string runNumber)
{
	DQMHistosBase::openFile(filePath_ + "/" + radixFileName_ + runNumber + ".root");
	DQMHistosBase::myDirectory_ = DQMHistosBase::theFile_->mkdir("PowerSupply", "PowerSupply");
	DQMHistosBase::myDirectory_->cd();

	__MOUT__ << "Creating Histograms" << std::endl;
	dqmHistos_ = new PowerSupplyDQMHistos();
	dqmHistos_->book(myDirectory_, theXDAQContextConfigTree_, theConfigurationPath_);
	DataConsumer::startProcessingData(runNumber);
	__MOUT__ << __PRETTY_FUNCTION__ << ": Done starting DQM!" << std::endl;

	// for (auto &device : theXDAQContextConfigTree_.getNode(theConfigurationPath_).getChildren())
	// {
	// 	std::cout << __PRETTY_FUNCTION__ << "Device: " << device.first << std::endl;
	// }
	/*
		DQMHistosBase::openFile(filePath_ + "/otsdaq_" + runNumber + ".root");
		TDirectory * MainDir           = DQMHistosBase::theFile_->mkdir("MainDir", "MainDir");        // <---
		MainDir->cd();

		hGaussA_ = new TH1F("GaussianMainA", "Gaussian mainA", 100, -50, +50);
		hCurrent = new TH1F("PowerSupplyCurrent", "PSCurrent", 1000, 0, 110);
		hVoltage = new TH1F("PowerSupplyVoltage", "PSVoltage", 100, 0, 10);
		hEfficy_ = new TH2F("Efficiency"   , "Efficiency plot", 52, 0, 52, 80, 0, 80);

		TDirectory * ParallelDir       = DQMHistosBase::theFile_->mkdir("ParallelDir", "ParallelDir");// <---
		ParallelDir->cd();
		hGaussB_ = new TH1F("GaussianMainB", "Gaussian mainB", 100, 0, 100);
	//        theCanvas_ = new TCanvas("canvas","canvas", 800,800) ;
	//        theCanvas_->Divide(2,1) ;

			TDirectory * ParallelSubDir    = ParallelDir->mkdir("ParallelSubDir", "ParallelSubDir");      // <---
		ParallelSubDir->cd();
		hGaussC_ = new TH1F("GaussianParallelSubDirC", "Gaussian ParallelSubC", 100, 0, 100);

			TDirectory * ParallelSubSubDir = ParallelSubDir->mkdir("ParallelSubSubDir", "MainSubSubDir"); // <---
		ParallelSubSubDir->cd();
		hGaussD_ = new TH1F("GaussianParallelSubSubDirD", "Gaussian ParallelSubSubDirD", 100, 0, 100);

		std::cout << __PRETTY_FUNCTION__ << "Starting!" << std::endl;
		DataConsumer::startProcessingData(runNumber);
		std::cout << __PRETTY_FUNCTION__ << "Started!" << std::endl;
	*/
}

//========================================================================================================================
void DQMPowerSupplyHistoConsumer::stopProcessingData(void)
{
	DataConsumer::stopProcessingData();
	if (saveFile_)
	{
		DQMHistosBase::save();
	}
	closeFile();
}

//========================================================================================================================
void DQMPowerSupplyHistoConsumer::pauseProcessingData(void)
{
	DataConsumer::stopProcessingData();
}

//========================================================================================================================
void DQMPowerSupplyHistoConsumer::resumeProcessingData(void)
{
	DataConsumer::startProcessingData("");
}

//========================================================================================================================
bool DQMPowerSupplyHistoConsumer::workLoopThread(toolbox::task::WorkLoop *workLoop)
{
	// std::cout << __COUT_HDR_FL__ << __PRETTY_FUNCTION__ << DataProcessor::processorUID_ << " running, because workloop: " << WorkLoop::continueWorkLoop_ << std::endl;
	fastRead();
	return WorkLoop::continueWorkLoop_;
}

//========================================================================================================================
void DQMPowerSupplyHistoConsumer::fastRead(void)
{
	if (DataConsumer::read(dataP_, headerP_) < 0)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
		return;
	}
	dqmHistos_->fill(*dataP_);
	if (saveFile_)
	{
		std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
			__MOUT__ << __E__;
		DQMHistosBase::autoSave();//Saves every 5 minutes
	}

	DataConsumer::setReadSubBuffer<std::string, std::map<std::string, std::string>>();

	// std::string json = dataP_->substr(4,dataP_->length()-4);
	//  auto histogramInformation = thePowerSupplyData_.convertFromJson(json);
	//  for (auto &group : histogramInformation)
	//  	{
	//      for (auto &channel : group.second)
	//      	{
	//  			std::cout<< histogramInformation[group.first][channel.first].current<< std::endl;
	//  			//hCurrent->Fill(histogramInformation[group.first][channel.first].current);
	//  			std::cout<< histogramInformation[group.first][channel.first].voltage<< std::endl;
	//  			//hVoltage->Fill(histogramInformation[group.first][channel.first].voltage);
	//  		}
	//  	}


	// std::cout << "Test size:" << test.length() << "-" << test << "-" << std::endl;
	//
	//  //std::cout<<"something2 " << histogramInformation["PowerSupplyData"]["LV_Module1"].current << std::endl;

	/*
		double value = 0;

		memcpy(&value, &dataP_->at(0), sizeof(double));
		std::cout << __LINE__ << "] [" << __PRETTY_FUNCTION__ << "] Value: " << value << std::endl;

			for(int bx=1; bx<=53; bx++)
			{
			 for(int by=1; by<=80; by++)
			 {
			  hEfficy_->SetBinContent(bx,by,random_->Gaus(99.6, 0.2)) ;
			 }
			}

			for(int i=0; i<random_->Gaus(10,4); ++i)
			{
		 hGaussA_->Fill(value-50);
		 hGaussB_->Fill(random_->Gaus(28,7));
		 hGaussC_->Fill(random_->Gaus(61,12));
			}

	*/

}

DEFINE_OTS_PROCESSOR(DQMPowerSupplyHistoConsumer)
