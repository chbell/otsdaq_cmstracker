#include "otsdaq-cmstracker/DataProcessorPlugins/DQMMiddlewareMonitorHistosConsumer.h"
#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/XmlUtilities/HttpXmlDocument.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/ProcessorPluginMacros.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/easylogging++.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/ConfigureInfo.h"
#include "otsdaq-cmstracker/Ph2_ACF/Parser/FileParser.h"
#include "otsdaq-cmstracker/Ph2_ACF/Parser/DetectorMonitorConfig.h"
#include "otsdaq-cmstracker/Ph2_ACF/MonitorDQM/MonitorDQMPlotBase.h"
#include "otsdaq-cmstracker/Ph2_ACF/MonitorDQM/MonitorDQMPlotCBC.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/ContainerSerialization.h"

#include <TDirectory.h>
#include <TFile.h>
#include <TTree.h>
#include <TGraph.h>
#include <TH1.h>
#include <TKey.h>
//#include <TROOT.h>

#include <chrono>
#include <thread>

INITIALIZE_EASYLOGGINGPP

using namespace ots;

//========================================================================================================================
DQMMiddlewareMonitorHistosConsumer::DQMMiddlewareMonitorHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: WorkLoop                    (processorUID)
	, DQMHistosConsumerBase       (supervisorApplicationUID, bufferUID, processorUID, LowConsumerPriority)
	, Configurable                (theXDAQContextConfigTree, configurationPath)
	, saveFile_                   (theXDAQContextConfigTree_.getNode(configurationPath).getNode("SaveFile").getValue<bool>())
	, filePath_                   (theXDAQContextConfigTree_.getNode(configurationPath).getNode("FilePath").getValue<std::string>())
	, radixFileName_              (theXDAQContextConfigTree_.getNode(configurationPath).getNode("RadixFileName").getValue<std::string>())
	, configurationDir_           (theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationDir").getValue<std::string>())
	, configurationName_          (theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationName").getValue<std::string>())
	, configurationFilePath_      (configurationDir_ + "/" + configurationName_)
	, moduleConfigurationFileName_("")
//, dqmh_                (0)

{
	//	gStyle->SetPalette(1);
}

//========================================================================================================================
DQMMiddlewareMonitorHistosConsumer::~DQMMiddlewareMonitorHistosConsumer(void)
{
	// DQMHistosBase::closeFile();
}
//========================================================================================================================
void DQMMiddlewareMonitorHistosConsumer::startProcessingData(std::string runNumber)
{
	fDetectorStructure.reset();
	
	Ph2_Parser::FileParser  fileParser;
	std::stringstream       out;
	Ph2_Parser::SettingsMap pSettingsMap;

	__MOUT__ << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Parsing file: " << configurationFilePath_ << std::endl;
	try
	{
		__MOUT__ << "[" << __LINE__ << "]" << "Trying to parse file: " << configurationFilePath_ << std::endl;
		fileParser.parseHW(configurationFilePath_, &fDetectorStructure, out);
		__MOUT__ << "[" << __LINE__ << "]" << "File parsed: " << configurationFilePath_ << std::endl;
	}
	catch (const std::exception &e)
	{
		__MOUT__ << "Error: " << e.what() << __E__;
		__COUT__ << "Error: " << e.what();
		throw std::runtime_error(e.what());
		return;
	}

	ConfigureInfo theConfigureInfo;

	__MOUT__ << "[" << __LINE__ << "]" << "Continuing..." << std::endl;
	try
	{
		moduleConfigurationFileName_ = __ENV__("MODULE_NAMES_FILE");
	}
	catch (std::runtime_error &e)
	{
		// If there is no environment, proceed without.
		// In fact MODULE_NAME_FILE must only be declared if the burnin is in use, and there the environment is checked if it exists
		__MOUT__ << "The following is a WARNING not an error. "
					<< "It just means that there is likely no file and it is not necessary to read it. "
					   "The file is necessary when running the burnin box but this might not be the case. ->"
					<< e.what() << __E__;
	}
	if(moduleConfigurationFileName_ != "")
	{
		HttpXmlDocument cfgXml;
		if(cfgXml.loadXmlDocument(moduleConfigurationFileName_))
		{
			std::vector<std::string> moduleLocation;
			std::vector<std::string> moduleId;
			std::vector<std::string> moduleName;
			cfgXml.getAllMatchingValues("ModuleLocation", moduleLocation);
			cfgXml.getAllMatchingValues("ModuleId",       moduleId);
			cfgXml.getAllMatchingValues("ModuleName",     moduleName);
			for (unsigned i=0; i<moduleName.size(); i++)
			{
				//std::cout << __PRETTY_FUNCTION__ << moduleLocation[i] << " : " << moduleName[i] << std::endl;
				if(moduleName[i] != "Empty")
					theConfigureInfo.enableOpticalGroup(0, atoi(moduleId[i].c_str()), moduleName[i]);
			}
		}
	}
	else
	{
		__SS__ << "Error. Failed to properly load file " + moduleConfigurationFileName_ + ". Make sure it exists and it well formatted. Try to write it again." << __E__;
		__SS_THROW__;
	}

	__MOUT__ << "[" << __LINE__ << "]" << "Continuing..." << std::endl;
	theConfigureInfo.setEnabledObjects(&fDetectorStructure);

	__MOUT__ << "[" << __LINE__ << "]" << "Continuing..." << std::endl;
    DetectorMonitorConfig theDetectorMonitorConfig;
    std::string           monitoringType = fileParser.parseMonitor(configurationFilePath_, theDetectorMonitorConfig, out);

	__MOUT__ << "[" << __LINE__ << "]" << "Continuing..." << std::endl;
	if(monitoringType == "2S") fMonitorDQMVector.push_back(new MonitorDQMPlotCBC());

	__MOUT__ << "[" << __LINE__ << "]" << "Continuing..." << std::endl;
	DQMHistosBase::openFile(filePath_ + "/" + radixFileName_ + runNumber + ".root");
	// TDirectoryFile* subDir = theFile_->mkdir("TrackerMonitor");
    for(auto monitorDQM: fMonitorDQMVector) monitorDQM->book(theFile_, fDetectorStructure, theDetectorMonitorConfig);

	__MOUT__ << "[" << __LINE__ << "]" << "Continuing..." << std::endl;
	fDataBuffer.clear();

	DataConsumer::startProcessingData(runNumber);
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << ": Done starting DQM!" << std::endl;
}

//========================================================================================================================
void DQMMiddlewareMonitorHistosConsumer::stopProcessingData(void)
{
	if (fDataBuffer.size() > 0)
	{
		std::cout << __PRETTY_FUNCTION__ << " Buffer should be empty, some data were not read, Aborting " << std::endl;
		abort();
	}

	{
		std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
		for (auto monitorDQM : fMonitorDQMVector)
			monitorDQM->process();
	}

	DataConsumer::stopProcessingData();
	if (saveFile_)
	{
		DQMHistosBase::save();
	}
	closeFile();
	for (auto monitorDQM : fMonitorDQMVector)
		delete monitorDQM;
	fMonitorDQMVector.clear();
}

//========================================================================================================================
void DQMMiddlewareMonitorHistosConsumer::pauseProcessingData(void)
{
	DataConsumer::stopProcessingData();
}

//========================================================================================================================
void DQMMiddlewareMonitorHistosConsumer::resumeProcessingData(void)
{
	DataConsumer::startProcessingData("");
}

//========================================================================================================================
bool DQMMiddlewareMonitorHistosConsumer::workLoopThread(toolbox::task::WorkLoop *workLoop)
{
	//	std::cout << __PRETTY_FUNCTION__ << DataProcessor::processorUID_ << " running, because workloop: " << WorkLoop::continueWorkLoop_ << std::endl;
	fastRead();
	return WorkLoop::continueWorkLoop_;
}

//========================================================================================================================
void DQMMiddlewareMonitorHistosConsumer::fastRead(void)
{
	// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "TRYING TO GET DATA!" << std::endl;
	if (DataConsumer::read(dataP_, headerP_) < 0)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		return;
	}
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;
	//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "GOT DATA AND Start Processing buffer block." << std::endl;

    PacketHeader thePacketHeader;
    uint8_t packerHeaderSize = thePacketHeader.getPacketHeaderSize();
	// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Got Something" << std::endl;
	fDataBuffer.insert(fDataBuffer.end(), dataP_->begin(), dataP_->end());
	while (fDataBuffer.size() > 0)
	{
		// std::cout << "fDataBuffer size: " << fDataBuffer.size() << std::endl;
		if (fDataBuffer.size() < packerHeaderSize)
		{
			// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << " " << __LINE__ << std::endl;
			break; // Not enough bytes to retreive the packet size
		}
		uint32_t packetSize = thePacketHeader.getPacketSize(fDataBuffer);
		// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << " Packet Number received " << int(theCurrentStream->getPacketNumber()) << std::endl;
		//std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << " vector size " << fDataBuffer.size() << " expected " << theCurrentStream->getPacketSize() << std::endl;
		if (fDataBuffer.size() < packetSize)
		{
			// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << " " << std::endl;
			break; // Packet not completed, waiting
		}
		std::string inputStream(fDataBuffer.begin() + packerHeaderSize, fDataBuffer.begin() + packetSize);
		fDataBuffer.erase(fDataBuffer.begin(), fDataBuffer.begin() + packetSize);

		if(inputStream == END_OF_TRANSMISSION_MESSAGE)
		{
			return;
		}

		for (auto monitorDQM : fMonitorDQMVector)
		{
			if (monitorDQM->fill(inputStream))
				break;
		}
		if (saveFile_)
		{
			std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
			__MOUT__ << __E__;
			DQMHistosBase::autoSave();//Saves every 5 minutes
		}
	}

	DataConsumer::setReadSubBuffer<std::string, std::map<std::string, std::string>>();
	//__MOUT__ << __LINE__ << __PRETTY_FUNCTION__ << ": Done Processing buffer block." << std::endl;
}

DEFINE_OTS_PROCESSOR(DQMMiddlewareMonitorHistosConsumer)
