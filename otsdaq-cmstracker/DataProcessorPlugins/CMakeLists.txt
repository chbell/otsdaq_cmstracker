include_directories(${CMAKE_BINARY_DIR}/artdaq)
include_directories($ENV{PH2ACF_BASE_DIR})
include_directories(${Protobuf_INCLUDE_DIRS})

link_directories(${Protobuf_LIBRARIES})

#get_cmake_property(_variableNames VARIABLES)
#foreach(_variableName ${_variableNames})
#	message(STATUS "${_variableName}=${${_variableName}}")
#endforeach()
cet_set_compiler_flags(
  EXTRA_FLAGS -D__OTSDAQ__
)

#MESSAGE(STATUS ${UHAL_UHAL_INCLUDE_PREFIX})

basic_plugin(DQMMiddlewareHistosConsumer "processor"
  Macros
  MF_MessageLogger
  ${CETLIB}
  Ph2_DQMUtils
  Ph2_Utils
  Ph2_MessageUtils
  Ph2_Parser
  NetworkUtils
  protobuf
  )

basic_plugin(DQMMiddlewareMonitorHistosConsumer "processor"
  Macros
  MF_MessageLogger
  ${CETLIB}
  Ph2_DQMUtils
  Ph2_Utils
  Ph2_MessageUtils
  Ph2_Parser
  Ph2_MonitorDQM
  NetworkUtils
  protobuf
  )

basic_plugin(DQMHistoMakerConsumer "processor"
  RootUtilities
  DataManager
  WorkLoopManager
  ConfigurationInterface
  Configurable
  ${CETLIB}
  TableCore
  MF_MessageLogger
  Macros
  ${XDAQ_BASIC_LIB_LIST}
  #${ROOT_BASIC_LIB_LIST}
 )

basic_plugin(DQMPowerSupplyHistoConsumer "processor"
  RootUtilities
  DataManager
  WorkLoopManager
  ConfigurationInterface
  Configurable
  OTUtilities
  OTDQMHistos
  ${CETLIB}
  TableCore
  MF_MessageLogger
  Macros
  ${XDAQ_BASIC_LIB_LIST}
  )

#basic_plugin(OTDQMHistosConsumer "processor"
#  RootUtilities
#  DataManager
#  WorkLoopManager
#  ConfigurationInterface
#  ${CETLIB}
#  Ph2_System_${Ph2_ACF_Master}
#  Ph2_Interface_${Ph2_ACF_Master}
#  Ph2_Utils_${Ph2_ACF_Master}
#  Ph2_NetworkUtils_${Ph2_ACF_Master}
#  Ph2_DQMUtils_${Ph2_ACF_Master}
#  Ph2_Tools_${Ph2_ACF_Master}
#  cactus_uhal_uhal
#  cactus_uhal_log
#  cactus_uhal_grammars 
#  )

install_headers()
install_source()
