#ifndef _ots_DQMMiddlewareMonitorHistosConsumer_h_
#define _ots_DQMMiddlewareMonitorHistosConsumer_h_

#include "otsdaq/DataManager/DQMHistosConsumerBase.h"
#include "otsdaq/Configurable/Configurable.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/Container.h"
#include <string>

class MonitorDQMPlotBase;
class TDirectory;

namespace ots
{

class DQMMiddlewareMonitorHistosConsumer : public DQMHistosConsumerBase, public Configurable
{
public:
  DQMMiddlewareMonitorHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~DQMMiddlewareMonitorHistosConsumer(void);

	void startProcessingData(std::string runNumber) override;
	void stopProcessingData (void) override;
	void pauseProcessingData (void) override;
	void resumeProcessingData(void) override;
	void load(std::string fileName){;}

private:
	bool workLoopThread(toolbox::task::WorkLoop* workLoop) override;
	void fastRead(void);
	
	//For fast read
	std::string*                               dataP_;
	std::map<std::string,std::string>*         headerP_;

	bool                                       saveFile_; //yes or no
	std::string                                filePath_;
	std::string                                radixFileName_;
	std::vector<MonitorDQMPlotBase*>           fMonitorDQMVector;
	std::vector<char>                          fDataBuffer;
	std::string                                configurationDir_;
	std::string                                configurationName_;
	std::string                                configurationFilePath_;
	std::string                                moduleConfigurationFileName_;
	DetectorContainer                          fDetectorStructure;
	std::atomic<bool>                          histogramsToReset_ {false};
};
}

#endif
