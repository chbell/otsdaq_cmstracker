#ifndef _ots_FEWROuterTrackerInterface_h_
#define _ots_FEWROuterTrackerInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include <string>
#include <memory>

//sleepMS()
#include <chrono>
#include <thread>

#include "../../../otsdaq/otsdaq-core/NetworkUtilities/UDPDataStreamerBase.h"
//MAPSA specific includes
#include "uhal/uhal.hpp"
using uhal::Node;              //HwInterface::getNode()
using uhal::ConnectionManager;
using uhal::HwInterface;
using uhal::ValWord;           //Node.read()
using uhal::ValHeader;         //Node.write()

namespace ots
{

class FEInterfaceConfigurationBase;
class FEWROuterTrackerInterfaceConfiguration;
class MPAPixelConfiguration;
class MPAPeripheryConfiguration;


class FEOuterTrackerInterface : public FEVInterface, public UDPDataStreamer/* , public OtsUDPHardware, public FSSRFirmware, public OtsUDPFirmware */
{
public:

	int getPixelCoordinate(size_t pixel);
	void writePixelAndPeripheryConfiguration();

	static void sleepMS(uint32_t ms)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(ms));
	}

public:
	FEOuterTrackerInterface     (std::string interfaceID, std::string interfaceType, const FEInterfaceConfigurationBase* configuration);
	virtual ~FEOuterTrackerInterface(void);

	void configure        (void);
	void halt             (void);
	void pause            (void);
	void resume           (void);
	void start            (std::string runNumber) override;
	void stop             (void);

	bool running          (void);

	int  universalRead	  (char* address, char* readValue) override  {return 0;}
	void universalWrite	  (char* address, char* writeValue) override {;}

	//void configureFEW     (void);
	void configureDetector(const DACStream& theDACStream) {;}




	//utility class used by the MAPSA class.
	class uasic
	{
	public:
		uasic(const std::string connection,	const std::string device)
	: manager_ (new ConnectionManager(connection))
	, hardware_(new HwInterface(manager_->getDevice(device)))
	//: manager_ (connection)
	//, hardware_(manager_.getDevice(device))
	{
			//std::cout << __PRETTY_FUNCTION__ << "CALLING THE CONSTRUCTOR!" << std::endl;
	}

		~uasic()
		{
			//std::cout << __PRETTY_FUNCTION__ << "CALLING THE DISTRUCTOR!" << std::endl;
			delete hardware_;
			delete manager_;
			hardware_ = 0;
			manager_  = 0;
		}



		HwInterface* getHardware() { return hardware_; }

	private:
		ConnectionManager* manager_;
		HwInterface*       hardware_;
	};

	//classes for the MAPSA and MPA. should separate these from this class
	//once it's confirmed that they are working.
	class MPA {
	public:

		MPA(HwInterface* hardware, int nmpa) :
			hardware_(hardware),
			nmpa_(nmpa)
	{ }

		void daq(void) { /* return MPA_daq(this->hardware, this->nmpa)*/ }

	private:
		HwInterface* hardware_;
		/*rename this (it's which mpa out of 6 the mpa is)*/
		int  nmpa_;

	};

	class MAPSA {
	public:

		MAPSA(uasic& u) :
			dev_(&u), //instance of the uasic class
			hardware_(dev_->getHardware()), //gets the mapsa hardware interface
			utility_(&hardware_->getNode("Utility"))
	{ }

		~MAPSA()
		{
			//    delete utility_;
		}

		int voltageWait(std::string supply, std::string state) {
			unsigned int x;
			if(state == "on") x = 0;
			else if (state == "off") x = 1;
			else {
				//wrong state
				return 0;
			}

			ValWord<uint32_t> read = utility_->getNode("MPA_settings_read").getNode(supply+"_enable").read();
			hardware_->dispatch();
			int count = 0;
			while(read == x) {
				sleepMS(5);
				read = utility_->getNode("MPA_settings_read").getNode(supply+"_enable").read();
				hardware_->dispatch();
				count += 1;
				if(count > 100) {
					std::cout << "Idle\n";
					return 0;
				}
			}
			return 1;
		}

		void VDDPST_on(void) {
			writeSettingsNode("VDDPST_enable", 0x1);
			sleepMS(1);
		}

		void DVDD_on(void) {
			writeSettingsNode("DVDD_enable", 0x1);
			sleepMS(1);
		}

		int AVDD_on(void) {
			writeSettingsNode("AVDD_enable", 0x1);
			sleepMS(1);
			/*why does this happen twice?*/
			writeSettingsNode("VBIAS_enable", 0x1);
			writeSettingsNode("VBIAS_enable", 0x1);
			std::cout << "VBIFEED on\n";
			writeUtilityNode("DAC_register", 0x12BE);
			sleepMS(1);
			std::cout << "VBIAS on\n";
			writeUtilityNode("DAC_register", 0x10BE);
			sleepMS(1);
			std::cout << "VBIPRE on\n";
			writeUtilityNode("DAC_register", 0x11BE);
			sleepMS(1);
			return 1;
		}

		void PVDD_on(void) {
			writeSettingsNode("PVDD_enable", 0x1);
			sleepMS(1);
		}

		void VBIAS_on(void) {
			writeSettingsNode("VBIAS_on", 0x1);
			sleepMS(1);
		}

		int VDDPST_off(void) {
			/*why does this happen twice*/
			writeSettingsNode("VDDPST_enable", 0x0);
			writeSettingsNode("VDDPST_enable", 0x0);
			sleepMS(1);
			return voltageWait("VDDPST", "off");
		}

		int DVDD_off(void) {
			/*why does this happen twice*/
			writeSettingsNode("DVDD_enable", 0x0);
			writeSettingsNode("DVDD_enable", 0x0);
			sleepMS(1);
			return voltageWait("DVDD", "off");
		}

		int AVDD_off(void) {
			/*why does this happen twice*/
			writeSettingsNode("AVDD_enable", 0x1);
			writeSettingsNode("AVDD_enable", 0x1);
			sleepMS(1);
			if(voltageWait("AVDD","off")) {
				std::cout << "VBIPRE off\n";
				writeUtilityNode("DAC_register", 0x1100);
				sleepMS(1);
				std::cout << "VBIAS off\n";
				writeUtilityNode("DAC_register", 0x1000);
				sleepMS(1);
				std::cout << "VBIFEED off\n";
				writeUtilityNode("DAC_register", 0x1200);
				sleepMS(1);
				return 1;
			}
			return 0;
		}

		int PVDD_off(void) {
			/*why does this happen twice*/
			writeSettingsNode("PVDD_enable", 0x0);
			writeSettingsNode("PVDD_enable", 0x0);
			sleepMS(1);
			return voltageWait("PVDD", "off");
		}

		int VBIAS_off(void) {
			writeSettingsNode("VBIAS_enable", 0x0);
			sleepMS(1);
			return voltageWait("VBIAS", "off");
		}

		/*maybe implement:
      safePowerOn(void)
      safePowerOff(void)
		 */

		void writeSettingsNode(const std::string& node, const uint32_t val) {
			//utility function since utility_->getNode("MPA_settings").getNode(...).write(...)
			//is relatively common
			utility_->getNode("MPA_settings").getNode(node).write(val);
			hardware_->dispatch();
		}
		void writeUtilityNode(const std::string& node, const uint32_t& val) {
			utility_->getNode(node).write(val);
			hardware_->dispatch();
		}
	private:
		uasic*       dev_;
		HwInterface* hardware_;
		const Node*  utility_;
	};

private:
	const FEWROuterTrackerInterfaceConfiguration* theConfiguration_;
	const MPAPixelConfiguration*                  thePixelConfiguration_;
	const MPAPeripheryConfiguration*              thePeripheryConfiguration_;
	uasic uasic_;
	MAPSA mapsa_;
	MPA   mpa_;



};



}

#endif
