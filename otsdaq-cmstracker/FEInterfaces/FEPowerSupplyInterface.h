#ifndef _ots_FEPowerSupplyInterface_h_
#define _ots_FEPowerSupplyInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include "otsdaq/NetworkUtilities/TCPPublishServer.h"
#include "otsdaq-cmstracker/OTUtilities/PowerSupplyData.h"

#include <string>

class PowerSupply;
class PowerSupplyChannel;

namespace ots
{
	class FEPowerSupplyInterface : public FEVInterface, public TCPPublishServer
	{
	public:
	public:
		FEPowerSupplyInterface(const std::string &interfaceUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath);
		virtual ~FEPowerSupplyInterface(void);

		void configure(void);
		void halt(void);
		void pause(void);
		void resume(void);
		void start(std::string runNumber) override;
		void stop(void);

		bool running(void);

		void universalRead(char *address, char *readValue) override { ; }
		void universalWrite(char *address, char *writeValue) override { ; }
		
		//MACROS
		void turnOn(__ARGS__);
		void turnOff(__ARGS__);


	private:
		bool turnOn(void);
		bool turnOff(void);
		std::vector<std::string> getListOfOnChannels(void);
		std::vector<std::string> getListOfOffChannels(void);
		struct ChannelPair
		{
			PowerSupplyChannel* channel          = nullptr;
			PowerSupplyChannel* channelToWaitFor = nullptr;
		};
		
		std::map<std::string, std::map<unsigned, ChannelPair>> turnOnSequenceMap_; // maps for turning on and off sequence, and the configured channels, as well as the powersupplies used.
		std::map<std::string, std::map<unsigned, ChannelPair>> turnOffSequenceMap_;
		std::map<std::string, std::map<std::string, PowerSupplyChannel *>> configuredChannelsMap_;
		std::map<std::string, PowerSupply *> powerSupplyMap_;
		PowerSupplyData thePowerSupplyData_;
	};

} // namespace ots
#endif