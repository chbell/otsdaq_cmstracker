#ifndef _ots_FEMiddlewareInterface_h_
#define _ots_FEMiddlewareInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include "otsdaq-cmstracker/Ph2_ACF/NetworkUtils/TCPClient.h"

#include <string>
#include <fstream>

namespace ots
{

class FEMiddlewareInterface : public FEVInterface, public TCPClient
{
public:

public:
	FEMiddlewareInterface(const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~FEMiddlewareInterface(void);

	void configure        (void);
	void halt             (void);
	void pause            (void);
	void resume           (void);
	void start            (std::string runNumber) override;
	void startIteration   (std::string runNumber, std::string append);
	void stop             (void);

	bool running          (void);

	void universalRead	  (char* address, char* readValue)  override  {;}
	void universalWrite	  (char* address, char* writeValue) override {;}


	void calibration                (__ARGS__);
	void waitForRunToCompleteAndStop(__ARGS__);
	
private:
    std::string sendCommand       (const std::string& command);
	void        checkReturnMessage(const std::string& message);

	std::string calibrationName_;
	std::string configurationDir_;
	std::string configurationName_;
	std::string configurationFilePath_;
	std::string moduleConfigurationFileName_;
	std::string runNumber_;
	int         iterationNumber_;
	int         secondsOfRunning_ {0};
	const int   MaxSecondsOfRunning_ {600};

};



}

#endif
