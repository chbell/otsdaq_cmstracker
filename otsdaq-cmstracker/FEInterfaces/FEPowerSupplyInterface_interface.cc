#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"
#include "otsdaq-cmstracker/FEInterfaces/FEPowerSupplyInterface.h"
#ifdef __CAEN__
#include "otsdaq-cmstracker/power_supply/src/CAEN.h" //This is done to prevent an error in compilation about windows.h
#endif
#include "otsdaq-cmstracker/power_supply/src/Keithley.h"
#include "otsdaq-cmstracker/power_supply/src/RohdeSchwarz.h"
#include "otsdaq-cmstracker/power_supply/src/TTi.h"
#include "otsdaq-cmstracker/power_supply/src/CAENelsFastPS.h"

#include <chrono>
#include <thread>

using namespace ots;

//========================================================================================================================
FEPowerSupplyInterface::FEPowerSupplyInterface(const std::string &interfaceUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: FEVInterface(interfaceUID, theXDAQContextConfigTree, configurationPath), TCPPublishServer(theXDAQContextConfigTree.getNode(configurationPath).getNode("DataPublisherPort").getValue<unsigned int>(), 1)
{
    // MACROS REGISTRATION
    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "TurnOnPowerSupplies",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FEPowerSupplyInterface::turnOn), // feMacroFunction
        std::vector<std::string>{}, //"},        // namesOfInputArgs
        std::vector<std::string>{},              // namesOfOutputArgs
        1);                                      // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "TurnOffPowerSupplies",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FEPowerSupplyInterface::turnOff), // feMacroFunction
        std::vector<std::string>{}, //"},        // namesOfInputArgs
        std::vector<std::string>{},              // namesOfOutputArgs
        1);                                      // requiredUserPermissions


	TCPPublishServer::startAccept();
	std::cout << __PRETTY_FUNCTION__ << "ConstructorDone!!!" << std::endl;
}

//========================================================================================================================
FEPowerSupplyInterface::~FEPowerSupplyInterface(void)
{
}

//========================================================================================================================
void FEPowerSupplyInterface::configure(void)
{
	std::cout << __PRETTY_FUNCTION__ << "Configure!" << std::endl;
	pugi::xml_document doc; // creates xml document and all of its components
	pugi::xml_node deviceNode = doc.append_child("Devices");
	pugi::xml_node powerSupplyNode;
	pugi::xml_node channelNode;

	for (auto &device : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable").getChildren())
	{
		if (theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable/" + device.second.getValueAsString() + "/LinkToPowerSupplyTable/Status").getValue() == "Off")
			continue;

		powerSupplyNode = deviceNode.append_child("PowerSupply");
		powerSupplyNode.append_attribute("ID").set_value(device.first.c_str()); // needs to use c string because of the definition on the function that does not accept strings
		for (auto &powerSupply : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable/" + device.second.getValueAsString() + "/LinkToPowerSupplyTable").getChildren())
		{
			if (powerSupply.first == "Status")
			{
				powerSupplyNode.append_attribute("InUse").set_value("Yes");
			}
			else if (powerSupply.first == "LinkToPowerSupplyChannelTable")
			{
				for (auto &channelGroup : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable/" + device.second.getValueAsString() + "/LinkToPowerSupplyTable/LinkToPowerSupplyChannelTable/").getChildren())
				{
					channelNode = powerSupplyNode.append_child("Channel");
					channelNode.append_attribute("ID").set_value(channelGroup.second.getValueAsString().c_str());
					for (auto &channel : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyDeviceTable/" + device.second.getValueAsString() + "/LinkToPowerSupplyTable/LinkToPowerSupplyChannelTable/" + channelGroup.second.getValueAsString()).getChildren())
					{
						if (channel.first == "LinkToPowerSupplyChannelTable" || channel.first == "CommentDescription" || channel.first == "Author" || channel.first == "RecordInsertionTime" || channel.first == "PowerSupplyGroupUID")
						{ //eliminates unwanted sections of the table
						}
						else
						{
							channelNode.append_attribute((channel.first).c_str()).set_value(channel.second.getValueAsString().c_str());
						}
					}
				}
			}
			else if (powerSupply.first != "CommentDescription" && powerSupply.first != "Author" && powerSupply.first != "RecordInsertionTime")
			{
				powerSupplyNode.append_attribute((powerSupply.first).c_str()).set_value(powerSupply.second.getValueAsString().c_str()); // appends values on powersupplynode
			}
		}
	}
	std::cout << "Saving result: " << doc.save_file("save_file_output.xml") << std::endl;												   //saves the result for checking purposes
	for (pugi::xml_node powerSupply = deviceNode.child("PowerSupply"); powerSupply; powerSupply = powerSupply.next_sibling("PowerSupply")) //checks for the type of powersupply
	{
		std::string inUse = powerSupply.attribute("InUse").value();
		if (inUse.compare("No") == 0 || inUse.compare("no") == 0)
			continue;
		std::string id    = powerSupply.attribute("ID").value();
		std::string model = powerSupply.attribute("Model").value();
		if (model.compare("CAENelsFastPS") == 0)
		{
			powerSupplyMap_.emplace(id, new CAENelsFastPS(powerSupply));
		}
#ifdef __CAEN__
		else if (model.compare("CAEN") == 0)
		{
			powerSupplyMap_.emplace(id, new CAEN(powerSupply));
		}
#endif
		else if (model.compare("TTi") == 0)
		{
			powerSupplyMap_.emplace(id, new TTi(powerSupply));
		}
		else if (model.compare("RohdeSchwarz") == 0)
		{
			powerSupplyMap_.emplace(id, new RohdeSchwarz(powerSupply));
		}
		else if (model.compare("Keithley") == 0)
		{
			powerSupplyMap_.emplace(id, new Keithley(powerSupply));
		}
		else
		{
			std::stringstream error;
			error << "The Model: " << model
				  << " is not an available power supply and won't be initialized, "
					 "please check the xml configuration file.";
			throw std::runtime_error(error.str());
		}
	}

	for (auto &channel : theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable").getChildren())
	{ 
		//get values from config tree for future use
		std::string group        = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/ChannelGroup"    ).getValue();
		bool     turnOnOff       = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/TurnOnOff"       ).getValue<bool>();
		if(!turnOnOff) continue;
		
		unsigned    turnOnSequence           = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/TurnOnSequence"  ).getValue<unsigned>();
		std::string turnOnWaitForChannelUID  = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/TurnOnWaitForChannelUID"  ).getValue();
		unsigned    turnOffSequence          = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/TurnOffSequence" ).getValue<unsigned>();
		std::string turnOffWaitForChannelUID = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/TurnOffWaitForChannelUID" ).getValue();
		std::string link                     = theXDAQContextConfigTree_.getNode(theConfigurationPath_ + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/LinkToChannelUID").getValue();
		
		for (auto &powerSupply : powerSupplyMap_)
		{
			for (auto &channel : powerSupply.second->getChannelList())
			{
				if (channel == link)
				{
					if(turnOnSequenceMap_[group].find(turnOnSequence) != turnOnSequenceMap_[group].end())
					{
						__FE_SS__ << "Channel group " << group << " has more than 1 element with turnOn sequence equal to " << turnOnSequence << __E__;
						__FE_SS_THROW__;
					}
					else
					{
						ChannelPair aPair;//Both are initialized with nullptr
						aPair.channel = powerSupply.second->getChannel(channel);
						if(turnOnWaitForChannelUID != "NONE")
						{
							try
							{
								aPair.channelToWaitFor = powerSupply.second->getChannel(turnOnWaitForChannelUID);//Will throw an exception if not found
							}
							catch(std::exception& e)
							{
								__FE_SS__ << e.what() << __E__;
								__FE_SS_THROW__;
							}
						}
						turnOnSequenceMap_[group][turnOnSequence] = aPair;
					}

					if (turnOffSequenceMap_[group].find(turnOffSequence) != turnOffSequenceMap_[group].end())
					{
						__FE_SS__ << "Channel group " << group << " has more than 1 element with turnOff sequence equal to " << turnOffSequence << __E__;
						__FE_SS_THROW__;
					}
					else
					{
						ChannelPair aPair;//Both are initialized with nullptr
						aPair.channel = powerSupply.second->getChannel(channel);
						if(turnOffWaitForChannelUID != "NONE")
						{
							try
							{
								aPair.channelToWaitFor = powerSupply.second->getChannel(turnOffWaitForChannelUID);//Will throw an exception if not found
							}
							catch(std::exception& e)
							{
								__FE_SS__ << e.what() << __E__;
								__FE_SS_THROW__;
							}
						}
						turnOffSequenceMap_[group][turnOffSequence] = aPair;
					}

					configuredChannelsMap_[group][channel] = powerSupply.second->getChannel(channel);
					thePowerSupplyData_.addChannel(group, channel);
				}
			}
		}
	}
}

//========================================================================================================================
void FEPowerSupplyInterface::halt(void)
{
	stop();
}

//========================================================================================================================
void FEPowerSupplyInterface::pause(void)
{
}

//========================================================================================================================
void FEPowerSupplyInterface::resume(void)
{
}

//========================================================================================================================
void FEPowerSupplyInterface::start(std::string runNumber)
{
	std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "start" << std::endl;												   //saves the result for checking purposes
	unsigned int i = VStateMachine::getIterationIndex();
	if(!turnOn())
	{
		std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Iteration: " << i << std::endl;												   //saves the result for checking purposes
		if(i<30)//30 seconds
		{
			VStateMachine::indicateIterationWork();
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			return;
		}
		else
		{
			std::vector<std::string> badChannels = getListOfOffChannels();
			if(badChannels.size() == 1)
			{
				__FE_SS__ << "Can NOT turn on channel: " << badChannels[0] << __E__;
				__FE_SS_THROW__;
			}
			else
			{
				__FE_SS__ << "Can NOT turn on these channels: ";
				for(auto channel: badChannels)
					ss << channel << " ";
				ss << __E__;
				__FE_SS_THROW__;
			}
		}
	}
}

//========================================================================================================================
//The running state is a thread
bool FEPowerSupplyInterface::running(void)
{
	for (auto &group : configuredChannelsMap_) // sets current, voltage and status values constantly, based on configuration values
	{
		for (auto &channel : group.second)
		{
			thePowerSupplyData_.setChannelStatus(group.first, channel.first, channel.second->getCurrent(), channel.second->getVoltage(), channel.second->isOn(), time(NULL));
		}
	}
	TCPPublishServer::broadcastPacket(thePowerSupplyData_.convertToJson()); //broadcast over tcp and goes to a consumer.
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	return WorkLoop::continueWorkLoop_; //otherwise it stops!!!!!
}

//========================================================================================================================
void FEPowerSupplyInterface::stop(void)
{
	unsigned int i = VStateMachine::getIterationIndex();
	if(!turnOff())
	{
		if(i<30)//30 seconds
		{
			VStateMachine::indicateIterationWork();
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			return;
		}
		else
		{
			std::vector<std::string> badChannels = getListOfOnChannels();
			if(badChannels.size() == 1)
			{
				__FE_SS__ << "Can NOT turn off channel: " << badChannels[0] << __E__;
				__FE_SS_THROW__;
			}
			else
			{
				__FE_SS__ << "Can NOT turn off these channels: ";
				for(auto channel: badChannels)
					ss << channel << " ";
				ss << __E__;
				__FE_SS_THROW__;
			}
		}
	}
}

//========================================================================================================================
bool FEPowerSupplyInterface::turnOn(void)
{
	for (auto channelGroup : turnOnSequenceMap_) //turns on based on the sequence determined on the config table, usually first LV1, then HV1, LV2, HV2, etc.
	{
		for (auto sequence : channelGroup.second)
		{
			std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ID: " << sequence.second.channel->getID() << " is on? " << sequence.second.channel->isOn() << " dependent on? " << sequence.second.channelToWaitFor->isOn() << std::endl;												   //saves the result for checking purposes
			if(!sequence.second.channel->isOn())
			{
				if(sequence.second.channelToWaitFor->isOn())
					sequence.second.channel->turnOn();
				else
					return false;
			}			
		}
	}
	return turnOnSequenceMap_.rbegin()->second.rbegin()->second.channel->isOn();
}

//========================================================================================================================
bool FEPowerSupplyInterface::turnOff(void)
{
	for (auto channelGroup : turnOffSequenceMap_) //turns on based on the sequence determined on the config table, usually first LV1, then HV1, LV2, HV2, etc.
	{
		for (auto sequence : channelGroup.second)
		{
			if(sequence.second.channel->isOn())
			{
				if(!sequence.second.channelToWaitFor->isOn())
					sequence.second.channel->turnOff();
				else
					return false;
			}			
		}
	}
	return !turnOnSequenceMap_.rbegin()->second.rbegin()->second.channel->isOn();
}

//========================================================================================================================
std::vector<std::string> FEPowerSupplyInterface::getListOfOnChannels(void)
{
	std::vector<std::string> channels;
	for (auto channelGroup : turnOnSequenceMap_) //turns on based on the sequence determined on the config table, usually first LV1, then HV1, LV2, HV2, etc.
	{
		for (auto sequence : channelGroup.second)
		{
			if(sequence.second.channel->isOn())
			{
				channels.push_back(sequence.second.channel->getID());
			}			
		}
	}
	return channels;
}
//========================================================================================================================
std::vector<std::string> FEPowerSupplyInterface::getListOfOffChannels(void)
{
	std::vector<std::string> channels;
	for (auto channelGroup : turnOffSequenceMap_) //turns on based on the sequence determined on the config table, usually first LV1, then HV1, LV2, HV2, etc.
	{
		for (auto sequence : channelGroup.second)
		{
			if(sequence.second.channel->isOn())
			{
				channels.push_back(sequence.second.channel->getID());
			}			
		}
	}
	return channels;
}

//========================================================================================================================
void FEPowerSupplyInterface::turnOn(__ARGS__)
{
    __FE_COUT__ << "Begin Macro TURN ON POWER SUPPLIES!!!!" << __E__;
    __FE_COUT__ << "Begin Macro TURN ON POWER SUPPLIES!!!!" << __E__;
	turnOn();
    __FE_COUT__ << "Done Macro TURN ON POWER SUPPLIES!!!!" << __E__;
    __FE_COUT__ << "Done Macro TURN ON POWER SUPPLIES!!!!" << __E__;
}

//========================================================================================================================
void FEPowerSupplyInterface::turnOff(__ARGS__)
{
    __FE_COUT__ << "Begin Macro TURN ON POWER SUPPLIES!!!!" << __E__;
    __FE_COUT__ << "Begin Macro TURN ON POWER SUPPLIES!!!!" << __E__;
	turnOff();
    __FE_COUT__ << "Done Macro TURN ON POWER SUPPLIES!!!!" << __E__;
    __FE_COUT__ << "Done Macro TURN ON POWER SUPPLIES!!!!" << __E__;
}

DEFINE_OTS_INTERFACE(FEPowerSupplyInterface)