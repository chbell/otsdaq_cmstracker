#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutHeaderMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"
#include "otsdaq-cmsoutertracker/FEInterfaces/FEMAPSAInterface.h"
//#include "otsdaq-cmsoutertracker/UserConfigurationDataFormats/FEMAPSAInterfaceConfiguration.h"

#include <iostream>
#include <algorithm>
#include <sstream>


using namespace ots;
INITIALIZE_EASYLOGGINGPP


//========================================================================================================================
FEMAPSAInterface::FEMAPSAInterface(const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath)
: FEVInterface       (interfaceUID, theXDAQContextConfigTree, configurationPath)
, UDPDataStreamerBase(
		theXDAQContextConfigTree_.getNode(configurationPath).getNode("HostIPAddress").getValue<std::string>(),
		theXDAQContextConfigTree_.getNode(configurationPath).getNode("HostPort").getValue<unsigned int>(),
		theXDAQContextConfigTree_.getNode(configurationPath).getNode("StreamToIPAddress").getValue<std::string>(),
		theXDAQContextConfigTree_.getNode(configurationPath).getNode("StreamToPort").getValue<unsigned int>())
, cHWFile_           (theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConnectionFile").getValue<std::string>())

{
}

//========================================================================================================================
FEMAPSAInterface::~FEMAPSAInterface (void)
{
}

//========================================================================================================================
void FEMAPSAInterface::configure(void)
{
	__MOUT__ << "using this file: " << cHWFile_ << std::endl;
	cSystemController.InitializeHw( cHWFile_ );
	//LOREcSystemController.ConfigureHw ( std::cout );
	MPAInterface* fMPAInterface = cSystemController.fMPAInterface;
	std::cout << "\nBOARD";
	BeBoard* pBoard = cSystemController.fBoardVector.at( 0 );
	std::vector < MPA* > fMPAVector;

	uint8_t pBeId = 0;
	uint8_t pFMCId = 0;
	uint8_t pFeId = 0;
	uint8_t pMPAId = 1;
	Module* MAPSA = new Module( pBeId, pFMCId, pFeId, 0 );

	//MPA *mpa1 = new MPA(pBeId, pFMCId, pFeId, pMPAId) ;
	//MAPSA->addMPA(mpa1);

	for (int i=0;i<12;i++)
		MAPSA->addMPA(new MPA(pBeId, pFMCId, pFeId, i));

	//std::cout << "\n"<<MAPSA->getNMPA();
	pBoard->addModule(MAPSA);
	std::cout << "\nExecuting POWER ON...";
	cSystemController.fBeBoardInterface->PowerOn(pBoard);
	std::cout << "\nFirmware version: ";
	cSystemController.fBeBoardInterface->ReadVer(pBoard);
	std::chrono::milliseconds cWait( 10 );

	for(int i=0;i<=5;i++)
	{

		std::this_thread::sleep_for( cWait );

		confsR_.push_back(fMPAInterface->ReadConfig("calibratedRight", i+1, 1));
		confsL_.push_back(fMPAInterface->ReadConfig("calibratedLeft", i+1, 1));
		std::pair < std::vector< std::string > ,std::vector< uint32_t >> mod1({"OM","THDAC"},{3,50});

		fMPAInterface->ModifyPerif(mod1,&confsR_[i]);
		fMPAInterface->ModifyPerif(mod1,&confsL_[i]);
		fMPAInterface->ConfigureMPA(&confsR_[i], 1 , i+1, 0);
		fMPAInterface->ConfigureMPA(&confsL_[i], 1 , i+1, 1);



	}

	fMPAInterface->SendConfig(6,6);
	std::chrono::milliseconds cWait1( 100 );//

}

//========================================================================================================================
void FEMAPSAInterface::halt (void)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	cSystemController.fBeBoardInterface->WriteBoardReg( cSystemController.fBoardVector.at( 0 ), "Control.testbeam_mode", 0 );
	cSystemController.fBeBoardInterface->WriteBoardReg( cSystemController.fBoardVector.at( 0 ), "Control.beam_on", 0 );
	if(outFile_.is_open())
		outFile_.close();
	//cSystemController.fBeBoardInterface->Stop( cSystemController.fBoardVector.at( 0 ));
}

//========================================================================================================================
void FEMAPSAInterface::pause (void)
{
} 

//========================================================================================================================
void FEMAPSAInterface::resume (void)
{
}

//========================================================================================================================
void FEMAPSAInterface::start (std::string runNumber)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	//std::string fileName = std::string(getenv("HOME")) + "/Data/Run_" + runNumber + ".raw";
	//std::string fileName = std::string(getenv("HOME")) + "/Data/Run_" + runNumber + ".raw";
	//cSystemController.addFileHandler( fileName, 'w' );
	//cSystemController.initializeFileHandler();
	std::cout << __PRETTY_FUNCTION__ << runNumber << std::endl;
	//cSystemController.fBeBoardInterface->Start( cSystemController.fBoardVector.at( 0 ) );
	spill_     = 0;
	tempspill_ = 0;
	nev_       = 0;
	ibuffer_   = 1;

	//fMPAInterface->SequencerInit(1,500000,1,0);
	MPAInterface* fMPAInterface = cSystemController.fMPAInterface;
	fMPAInterface->TestbeamInit(500000,0, 0);

	std::cout<<"Clearing buffers"<<std::endl;
	for(int i=0;i<=1;i++)
	{
		for(int k=1;k<=4;k++)
		{
			for(int j=1;j<=6;j++)
			{
				fMPAInterface->HeaderInitMPA(j,i);
				std::pair<std::vector<uint32_t>, std::vector<uint32_t>>  returndata = fMPAInterface->ReadMPAData(k,j,i);
			}
			fMPAInterface->ReadTrig(k);
		}
	}
	fMPAInterface->Cleardata();

	std::string fileName = "Run" + runNumber + "MAPSA_Raw.dat";
	std::cout << __COUT_HDR_FL__ << "Saving file: " << fileName << std::endl;
	outFile_.open(fileName.c_str(), std::ios::out | std::ios::binary);
	if(!outFile_.is_open())
	{
		std::cout << __COUT_HDR_FL__ << "Can't open file " << fileName << std::endl;
		assert(0);
	}

}

//========================================================================================================================
//The running state is a thread
bool FEMAPSAInterface::running(void)
{
	//std::cout << __PRETTY_FUNCTION__ << "Begin!" << std::endl;

	MPAInterface* fMPAInterface = cSystemController.fMPAInterface;
	std::cout << "\nBOARD";
	BeBoard* pBoard = cSystemController.fBoardVector.at( 0 );

	tempspill_ = spill_;
	int time = 0;
	while(fMPAInterface->WaitTestbeam() >= 4 && WorkLoop::continueWorkLoop_)
	{
		if(time++%1000 == 0)
			__MOUT__ << "Waiting for spill: " << time/1000 << " seconds" << std::endl;
		usleep(1000);
	}
	spill_++;
	if (tempspill_ != spill_) std::cout << "Starting Spill " << spill_ << std::endl;

	fMPAInterface->Cleardata();
	fMPAInterface->ReadTrig(ibuffer_);



	for(int i=0;i<=5;i++)
	{
		for(int j=0;j<=1;j++)
		{
			std::pair<std::vector<uint32_t>, std::vector<uint32_t>>  returndata = fMPAInterface->ReadMPAData(ibuffer_,i+1,j);
		}
	}

	ibuffer_ += 1;
	if (ibuffer_ > 4) ibuffer_ = 1;

	std::vector<uint32_t> cData = *(fMPAInterface->GetcurData());
	//uint32_t cPacketSize = cSystemController.fBeBoardInterface->ReadData ( pBoard, false, cData);
	int iic1 = 0;
	for( unsigned int iic1=0;iic1<cData.size();iic1++)
	{
		outFile_.write( (char*)&cData.at(iic1), sizeof(uint32_t));
		//std::cout<<iic1<<" "<< cData.at(iic1) <<std::endl;
	}

	//const std::vector<Event*>& events = cSystemController.GetEvents( cSystemController.fBoardVector.at( 0 ) );

	if (cData.size()!=0)
	{
		//		std::cout << __PRETTY_FUNCTION__ << ">>> CBCInterface Recorded Events # " << events.size() << std::endl;
		//		for ( auto& ev : events )
		//		{
		//			std::cout << __PRETTY_FUNCTION__<< "Event #:" << count++ << std::endl;
		//	        std::cout << *ev << std::endl;
		////	        std::cout << "COMPARING$$$$$$$$$$$$$$$$$$$$$$$$"<<std::endl;
		////			//if ( count % 100  == 0 )
		////	//			std::cout << ">>> Recorded Event #" << count << std::endl;
		//		}

		//std::cout << __PRETTY_FUNCTION__ << "Streaming data..." << std::endl;
		//    std::cout << __PRETTY_FUNCTION__ << "cData " << cData.size() << " nEvents " << events.size() << " event " << events.size() << std::endl;
		TransmitterSocket::send(streamToSocket_, cData);
	}

	nev_ += 1;
	if (nev_%100 == 0)	std::cout << nev_ << " Events" <<std::endl;
	/*
        const std::vector<Event*>* pEvents ;
        pEvents = &mysyscontroller.GetEvents ( pBoard );



        uint32_t total_trigs = (pEvents->at(0))->Gettotal_trigs();
        uint32_t trigger_total_counter = (pEvents->at(0))->Gettrigger_total_counter();
        uint32_t trigger_counter = (pEvents->at(0))->Gettrigger_counter() ;
	std::vector<uint32_t> trigger_offset_BEAM = (pEvents->at(0))->Gettrigger_offset_BEAM() ;
	std::vector<uint32_t> trigger_offset_MPA =(pEvents->at(0))->Gettrigger_offset_MPA() ;

	std::cout<<"total triggers"<<std::endl;
 	std::cout<<total_trigs<<std::endl;
	std::cout<<"trigger counter"<<std::endl;
	std::cout<<trigger_counter<<std::endl;
	std::cout<<"trigger total counter"<<std::endl;
        std::cout<<trigger_total_counter<<std::endl;
	std::cout<<"trigger_offset_BEAM"<<std::endl;
	int iic1 = 0;
        for( auto &vv : trigger_offset_BEAM)
		{
		if (iic1>50) break;
		std::bitset<32> p(vv);
		std::cout<<iic1++<<"  "<<p.to_string()<<std::endl;
                }

	std::cout<<"trigger_offset_MPA"<<std::endl;

	int iic2 = 0;
        for( auto &vv : trigger_offset_MPA)
		{

		std::bitset<32> p(vv);
		std::cout<<iic2++<<"  "<<p.to_string()<<std::endl;
                }


	if (ibuffer!=1) continue;
	const EventDataMap &EDM =(pEvents->at(0))->GetEventDataMap();
	for (auto &ev: EDM)
		{
			if (ev.first!= 1) continue;
			std::cout<<ev.first<<std::endl;

			int iic = 0;
                        for( auto &vv : ev.second) {
			      if (iic >75) continue;
		    	      std::bitset<32> p(vv);

                              std::cout<<iic++<<"  "<<p.to_string()<<std::endl;
                        }
		}

	std::cout<<PacketSize<<std::endl;
	 */

	return WorkLoop::continueWorkLoop_;//otherwise it stops!!!!!
}

//========================================================================================================================
void FEMAPSAInterface::stop (void)
{
	cSystemController.fBeBoardInterface->WriteBoardReg( cSystemController.fBoardVector.at( 0 ), "Control.testbeam_mode", 0 );
	cSystemController.fBeBoardInterface->WriteBoardReg( cSystemController.fBoardVector.at( 0 ), "Control.beam_on", 0 );
	if(outFile_.is_open())
		outFile_.close();
}

DEFINE_OTS_INTERFACE(FEMAPSAInterface)
