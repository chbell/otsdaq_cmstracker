#ifndef _ots_FEWRCBCInterface_h_
#define _ots_FEWRCBCInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include "otsdaq/NetworkUtilities/UDPDataStreamerBase.h"
#include "otsdaq-cmsoutertracker/Ph2_ACF/System/SystemController.h"

#include <string>
#include <fstream>

namespace ots
{

class FEMAPSAInterface : public FEVInterface, public UDPDataStreamerBase
{
public:

public:
	FEMAPSAInterface(const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath);
	virtual ~FEMAPSAInterface(void);

	void configure        (void);
	void halt             (void);
	void pause            (void);
	void resume           (void);
	void start            (std::string runNumber) override;
	void stop             (void);

	bool running          (void);

	int universalRead	  (char* address, char* readValue) override  {return 0;}
	void universalWrite	  (char* address, char* writeValue) override {;}

	//THESE 2 DON'T NEED TO BE IMPLEMENTED!!!
	//void configureFEW     (void);
	//void configureDetector(const DACStream& theDACStream) {;}
	////////////////////////////////////////////////////////////



private:
	std::string cHWFile_;
	Ph2_System::SystemController cSystemController;
	int spill_;
	int tempspill_;
	int nev_;
	int ibuffer_;
	std::vector<std::vector< uint32_t >> confsL_;
	std::vector<std::vector< uint32_t >> confsR_;
	std::ofstream                      outFile_;
};



}

#endif
