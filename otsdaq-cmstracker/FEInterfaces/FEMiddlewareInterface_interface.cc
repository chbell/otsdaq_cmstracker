#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"
#include "otsdaq-cmstracker/FEInterfaces/FEMiddlewareInterface.h"
#include "otsdaq/XmlUtilities/HttpXmlDocument.h"

#include "otsdaq-cmstracker/Ph2_ACF/Utils/easylogging++.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/ConfigureInfo.h"
#include "otsdaq-cmstracker/Ph2_ACF/Utils/StartInfo.h"
#include "otsdaq-cmstracker/Ph2_ACF/MessageUtils/cpp/QueryMessage.pb.h"
#include "otsdaq-cmstracker/Ph2_ACF/MessageUtils/cpp/ReplyMessage.pb.h"

#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>

using namespace ots;
INITIALIZE_EASYLOGGINGPP

//========================================================================================================================
FEMiddlewareInterface::FEMiddlewareInterface(const std::string &interfaceUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath)
	: FEVInterface(interfaceUID, theXDAQContextConfigTree, configurationPath), TCPClient(
																				   theXDAQContextConfigTree_.getNode(configurationPath).getNode("InterfaceIPAddress").getValue<std::string>(),
																				   theXDAQContextConfigTree_.getNode(configurationPath).getNode("InterfacePort").getValue<unsigned int>())

	  ,
	  calibrationName_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("CalibrationName").getValue<std::string>()), configurationDir_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationDir").getValue<std::string>()), configurationName_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConfigurationName").getValue<std::string>()), configurationFilePath_(configurationDir_ + "/" + configurationName_), moduleConfigurationFileName_(""), iterationNumber_(-1)
{

	FEVInterface::registerFEMacroFunction(
		"Calibration", // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(
			&FEMiddlewareInterface::calibration),	 // feMacroFunction
		std::vector<std::string>{"CalibrationType"}, // calibrationandpedenoise"},             // namesOfInputArgs
		std::vector<std::string>{},					 // namesOfOutputArgs
		1);											 // requiredUserPermissions

	FEVInterface::registerFEMacroFunction(
		"WaitForRunToCompleteAndStop", // feMacroName
		static_cast<FEVInterface::frontEndMacroFunction_t>(
			&FEMiddlewareInterface::waitForRunToCompleteAndStop), // feMacroFunction
		std::vector<std::string>{},								  // namesOfInputArgs
		std::vector<std::string>{},								  // namesOfOutputArgs
		1);														  // requiredUserPermissions

} // end constructor

//========================================================================================================================
FEMiddlewareInterface::~FEMiddlewareInterface(void)
{
} // end destructor

//========================================================================================================================
void FEMiddlewareInterface::configure(void)
{
	__FE_COUT__ << "Configure" << std::endl;

	try
	{
		TCPClient::connect(30, 1000); // Try for 30 seconds and then give up
	}
	catch (const std::exception &e)
	{
		__FE_SS__ << e.what() << __E__;
		__FE_SS_THROW__; // -->> IT DOESN'T SEEM TO PERCOLATE TO THE GATEWAY WINDOW ERROR
	}

	// const google::protobuf::EnumDescriptor* fCalibrationEnumDescriptor = MessageUtils::CalibrationList_CalibrationNameEnum_descriptor();
	// const auto           theCalibrationEnum = static_cast<MessageUtils::CalibrationList::CalibrationNameEnum>(fCalibrationEnumDescriptor->FindValueByName(calibrationName_)->number());
	// MessageUtils::ConfigurationMessage theQuery;
	// theQuery.mutable_query_type()->set_type(MessageUtils::QueryType::CONFIGURE);
	// theQuery.mutable_data()->mutable_calibration()->set_calibration_name(theCalibrationEnum);
	// theQuery.mutable_data()->set_configuration_file(configurationFilePath_);
	// std::string theCommandString;
	// theQuery.SerializeToString(&theCommandString);

	// checkReturnMessage(sendCommand(theCommandString));

	ConfigureInfo theConfigureInfo;
	theConfigureInfo.setConfigurationFile(configurationFilePath_);
	theConfigureInfo.setCalibrationName(calibrationName_);
	try
	{
		moduleConfigurationFileName_ = __ENV__("MODULE_NAMES_FILE");
	}
	catch (std::runtime_error &e)
	{
		// If there is no environment, proceed without.
		// In fact MODULE_NAME_FILE must only be declared if the burnin is in use, and there the environment is checked if it exists
		__FE_COUT__ << "The following is a WARNING not an error. "
					<< "It just means that there is likely no file and it is not necessary to read it. "
					   "The file is necessary when running the burnin box but this might not be the case. ->"
					<< e.what() << __E__;
	}
	if(moduleConfigurationFileName_ != "")
	{
		HttpXmlDocument cfgXml;
		if(cfgXml.loadXmlDocument(moduleConfigurationFileName_))
		{
			std::vector<std::string> moduleLocation;
			std::vector<std::string> moduleId;
			std::vector<std::string> moduleName;
			cfgXml.getAllMatchingValues("ModuleLocation", moduleLocation);
			cfgXml.getAllMatchingValues("ModuleId",       moduleId);
			cfgXml.getAllMatchingValues("ModuleName",     moduleName);
			for (unsigned i=0; i<moduleName.size(); i++)
			{
				std::cout << __PRETTY_FUNCTION__ << moduleLocation[i] << " : " << atoi(moduleId[i].c_str()) << " : " << moduleName[i] << std::endl;
				if(moduleName[i] != "Empty")
					theConfigureInfo.enableOpticalGroup(0, atoi(moduleId[i].c_str()), moduleName[i]);
			}
		}
	}
	else
	{
		__FE_SS__ << "Error. Failed to properly load file " + moduleConfigurationFileName_ + ". Make sure it exists and it well formatted. Try to write it again." << __E__;
		__FE_SS_THROW__;
	}

	std::string theCommandString = theConfigureInfo.createProtobufMessage();
	checkReturnMessage(sendCommand(theCommandString));

	// std::string readBuffer = TCPClient::sendAndReceivePacket("Configure,Calibration:" + calibrationName_ + ",ConfigurationFile:" + configurationFilePath);
	//__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::halt(void)
{
	iterationNumber_ = -1;
	__FE_COUT__ << std::endl;
	MessageUtils::QueryMessage theQuery;
	theQuery.mutable_query_type()->set_type(MessageUtils::QueryType::HALT);
	std::string theCommandString;
	theQuery.SerializeToString(&theCommandString);

	checkReturnMessage(sendCommand(theCommandString));
}

//========================================================================================================================
void FEMiddlewareInterface::pause(void)
{
	__FE_COUT__ << std::endl;
	__FE_COUT__ << "Not sending anything for PAUSE!" << std::endl;
	// std::string readBuffer;
	// readBuffer = TCPClient::sendAndReceivePacket("Pause");
	// __FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::resume(void)
{
	__FE_COUT__ << std::endl;
	__FE_COUT__ << "Not sending anything for RESUME!" << std::endl;
	// std::string readBuffer;
	// readBuffer = TCPClient::sendAndReceivePacket("Resume");
	// __FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::start(std::string runNumber)
{
	__FE_COUT__ << "Starting..." << std::endl;
	runNumber_ = runNumber;
	std::string iterationValue;
	if(iterationNumber_<0) iterationValue = "start";
	else iterationValue = std::to_string(iterationNumber_);
	++iterationNumber_;
	std::string iterationString = "Iteration_" + iterationValue;

	StartInfo theStartInfo;
	theStartInfo.setRunNumber(stoi(runNumber_));
	theStartInfo.setAppendInformation(iterationString);

	std::string theCommandString = theStartInfo.createProtobufMessage();
	checkReturnMessage(sendCommand(theCommandString));

	// TCPClient::sendAndReceivePacket("Start:{RunNumber:" + runNumber + "}");
	__FE_COUT__ << "Done Starting..." << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::startIteration(std::string runNumber, std::string append)
{
	__FE_COUT__ << "Starting Iteration..." << std::endl;
	// TCPClient::sendAndReceivePacket("Iteration:{RunNumber:" + runNumber + ",Append:" + append + "}");
	__FE_COUT__ << "Done Starting Iteration..." << std::endl;
}

//========================================================================================================================
// The running state is a thread
bool FEMiddlewareInterface::running(void)
{
	__FE_COUT__ << "running " << std::endl;
	// std::string readBuffer;
	// readBuffer = TCPClient::sendAndReceivePacket("Status?");

	MessageUtils::QueryMessage theQuery;
	theQuery.mutable_query_type()->set_type(MessageUtils::QueryType::STATUS);
	std::string theCommandString;
	theQuery.SerializeToString(&theCommandString);
	std::string readBuffer = sendCommand(theCommandString);
	__FE_COUT__ << "Message received: " << readBuffer << std::endl;

	MessageUtils::ReplyMessage theStatus;
	theStatus.ParseFromString(readBuffer);

	switch (theStatus.reply_type().type())
	{
	case MessageUtils::ReplyType::RUNNING:
	{
		std::cout << __PRETTY_FUNCTION__ << "Still running..." << __E__;
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		++secondsOfRunning_;
		if (secondsOfRunning_ > MaxSecondsOfRunning_)
		{
			std::cout << __PRETTY_FUNCTION__ << "Something whent wrong with the Ph2_ACF, still in running state after " << MaxSecondsOfRunning_ << " seconds, going to the next step" << __E__;
			WorkLoop::continueWorkLoop_ = false;
		}
		break;
	}
	case MessageUtils::ReplyType::SUCCESS:
	{
		WorkLoop::continueWorkLoop_ = false;
		break;
	}
	case MessageUtils::ReplyType::ERROR:
	{
		__FE_COUT__ << "I got an Error from RunController: " << theStatus.message();
		break;
	}
	default:
	{
		__FE_COUT__ << "Invalid reply received: " << readBuffer << __E__;
	}
	}
	// std::this_thread::sleep_for(std::chrono::seconds(5));
	// WorkLoop::continueWorkLoop_ = false;
	return WorkLoop::continueWorkLoop_; // otherwise it stops!!!!!
}

//========================================================================================================================
void FEMiddlewareInterface::stop(void)
{
	__FE_COUT__ << std::endl;
	MessageUtils::QueryMessage theQuery;
	theQuery.mutable_query_type()->set_type(MessageUtils::QueryType::STOP);
	std::string theCommandString;
	theQuery.SerializeToString(&theCommandString);
	std::string readBuffer = sendCommand(theCommandString);

	// std::string readBuffer;
	// readBuffer = TCPClient::sendAndReceivePacket("Stop");
	secondsOfRunning_ = 0;
	//__FE_COUT__ << "Message received: " << readBuffer << std::endl;
}

//========================================================================================================================
void FEMiddlewareInterface::calibration(__ARGS__)
{
	std::cout << __PRETTY_FUNCTION__ << "# of input args = " << argsIn.size() << __E__;
	std::cout << __PRETTY_FUNCTION__ << "# of output args = " << argsOut.size() << __E__;
	for (auto &argIn : argsIn)
		std::cout << __PRETTY_FUNCTION__ << argIn.first << ": " << argIn.second << __E__;

	std::cout << __PRETTY_FUNCTION__ << "Workloop status: " << WorkLoop::continueWorkLoop_ << __E__;
	while (WorkLoop::continueWorkLoop_ == true)
	{
		std::cout << __PRETTY_FUNCTION__ << "Still running from previous command. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}

	std::string calibrationType = __GET_ARG_IN__("CalibrationType", std::string);
	//if (calibrationType == "calibrationandpedenoise")
	//{
		std::cout << __PRETTY_FUNCTION__ << "Doing " << calibrationType << __E__;		
		
		//halt();
		calibrationName_ = calibrationType;
		// std::string configurationFilePath = configurationDir_ + "/" + configurationName_;
		// std::string readBuffer = TCPClient::sendAndReceivePacket("Configure,Calibration:" + calibrationName_ + ",ConfigurationFile:" + configurationFilePath);
		// std::cout << __PRETTY_FUNCTION__ << "Message received: " << readBuffer << std::endl;
		// configure(); // commented out because "ERROR: This client is already connected. This must never happens. It probably means that the connect method is called multiple times before the TCPClient has been disconnected."
		std::cout << __PRETTY_FUNCTION__ << "Starting Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::cout << __PRETTY_FUNCTION__ << "Starting Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::cout << __PRETTY_FUNCTION__ << "Starting Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::cout << __PRETTY_FUNCTION__ << "Starting Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;

		// // Fabio - TMP - begin
		// if(iterationNumber_%2 != 0)
		// {
		//     int waitAtTargetTime = 600;
		// 	int checkTime = 5; //seconds

		// 	while(waitAtTargetTime>0) //should it be an atomic?
		// 	{
		// 		__FE_COUT__ << "Waiting at target for other " << waitAtTargetTime << " seconds " << __E__;
		// 		std::this_thread::sleep_for (std::chrono::seconds(checkTime));
		// 		waitAtTargetTime-=checkTime;
		// 	}
		// }
		// // Fabio - TMP - end

		// std::string appendString = "Iteration_" + std::to_string(iterationNumber_++);
		// startIteration(runNumber_, appendString);
		start(runNumber_);
		WorkLoop::continueWorkLoop_ = true;
		while (WorkLoop::continueWorkLoop_ == true)
		{
			std::cout << __PRETTY_FUNCTION__ << "Still running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
			std::cout << __PRETTY_FUNCTION__ << "Still running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
			std::cout << __PRETTY_FUNCTION__ << "Still running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
			std::cout << __PRETTY_FUNCTION__ << "Still running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
			running();
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
		stop();
		std::cout << __PRETTY_FUNCTION__ << "Done with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::cout << __PRETTY_FUNCTION__ << "Done with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::cout << __PRETTY_FUNCTION__ << "Done with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	// }
	// else
	// {
	// 	__FE_SS__ << "Invalid calibration type " << calibrationType << __E__;
	// 	__FE_SS_THROW__;
	// }

} // end calibration()

//========================================================================================================================
void FEMiddlewareInterface::waitForRunToCompleteAndStop(__ARGS__)
{

	std::cout << __PRETTY_FUNCTION__ << "Workloop status: " << WorkLoop::continueWorkLoop_ << __E__;
	while (WorkLoop::continueWorkLoop_ == true)
	{
		std::cout << __PRETTY_FUNCTION__ << "Still running from previous command. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
	while (WorkLoop::continueWorkLoop_ == true)
	{
		std::cout << __PRETTY_FUNCTION__ << "Still running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::cout << __PRETTY_FUNCTION__ << "Still running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::cout << __PRETTY_FUNCTION__ << "Still running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		std::cout << __PRETTY_FUNCTION__ << "Still running...Macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
		running();
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
	stop();
	std::cout << __PRETTY_FUNCTION__ << "Done with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	std::cout << __PRETTY_FUNCTION__ << "Done with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
	std::cout << __PRETTY_FUNCTION__ << "Done with macro. Workloop: " << WorkLoop::continueWorkLoop_ << __E__;
}

//========================================================================================================================
std::string FEMiddlewareInterface::sendCommand(const std::string &command)
{
	try
	{
		return TCPClient::sendAndReceivePacket(command);
	}
	catch (const std::exception &e)
	{
		throw(std::runtime_error(e.what()));
		return "";
	}
}

//========================================================================================================================
void FEMiddlewareInterface::checkReturnMessage(const std::string &message)
{
	MessageUtils::ReplyMessage theReturnedMessage;
	theReturnedMessage.ParseFromString(message);
	if (theReturnedMessage.reply_type().type() == MessageUtils::ReplyType::ERROR)
		throw(std::runtime_error(theReturnedMessage.message()));
	else if (theReturnedMessage.reply_type().type() == MessageUtils::ReplyType::SUCCESS)
		__FE_COUT__ << "SUCCESS: Message received: " << message << std::endl;
	else if (theReturnedMessage.reply_type().type() == MessageUtils::ReplyType::RUNNING)
		__FE_COUT__ << "RUNNING: Message received: " << message << std::endl;
	else
		__FE_COUT__ << "Message received with reply type number: " << message << std::endl;
}

DEFINE_OTS_INTERFACE(FEMiddlewareInterface)
