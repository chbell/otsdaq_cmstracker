#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"
#include "otsdaq-cmsoutertracker/FEInterfaces/FECBCInterface.h"
//#include "otsdaq-cmsoutertracker/UserConfigurationDataFormats/FECBCInterfaceConfiguration.h"

#include <iostream>
#include <algorithm>
#include <sstream>


using namespace ots;
INITIALIZE_EASYLOGGINGPP


//========================================================================================================================
FECBCInterface::FECBCInterface(const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath)
:Socket              (
		theXDAQContextConfigTree.getNode(configurationPath).getNode("HostIPAddress").getValue<std::string>()
		,theXDAQContextConfigTree.getNode(configurationPath).getNode("HostPort").getValue<unsigned int>())
, FEVInterface       (interfaceUID, theXDAQContextConfigTree, configurationPath)
, UDPDataStreamerBase(
		theXDAQContextConfigTree_.getNode(configurationPath).getNode("HostIPAddress").getValue<std::string>(),
		theXDAQContextConfigTree_.getNode(configurationPath).getNode("HostPort").getValue<unsigned int>(),
		theXDAQContextConfigTree_.getNode(configurationPath).getNode("StreamToIPAddress").getValue<std::string>(),
		theXDAQContextConfigTree_.getNode(configurationPath).getNode("StreamToPort").getValue<unsigned int>())
, cHWFile_           (theXDAQContextConfigTree_.getNode(configurationPath).getNode("ConnectionFile").getValue<std::string>())
, saveDataFile_      (theXDAQContextConfigTree_.getNode(configurationPath).getNode("SaveDataFile").getValue<bool>())
, dataFilePath_      (theXDAQContextConfigTree_.getNode(configurationPath).getNode("DataFilePath").getValue<std::string>())
, dataFileNamePrefix_(theXDAQContextConfigTree_.getNode(configurationPath).getNode("DataFileNamePrefix").getValue<std::string>())
, eventCounter_      (0)

{
	std::cout << "Host IP: " <<	theXDAQContextConfigTree_.getNode(configurationPath).getNode("HostIPAddress").getValue<std::string>()
				<< " Host Port: " << theXDAQContextConfigTree_.getNode(configurationPath).getNode("HostPort").getValue<unsigned int>()
				<< " Strem IP: " << theXDAQContextConfigTree_.getNode(configurationPath).getNode("StreamToIPAddress").getValue<std::string>()
				<< " Stream Port: " << theXDAQContextConfigTree_.getNode(configurationPath).getNode("StreamToPort").getValue<unsigned int>()
				<< std::endl;
}

//========================================================================================================================
FECBCInterface::~FECBCInterface (void)
{
}

//========================================================================================================================
void FECBCInterface::configure(void)
{
	__MOUT__ << "Middleware configuration file: " << cHWFile_ << std::endl;
    // Check if file exists, otherwise throw runtime error
    {
        std::ifstream hwinfile;
        hwinfile.exceptions(std::ifstream::failbit);
        try{
                hwinfile.open(cHWFile_);
        }
        catch (const std::exception& e){
            std::ostringstream msg;
            msg << "Middleware configuration file does not exist.";
            throw std::runtime_error(msg.str());
        }
    }

	//FIXME THIS WAS ADDED FOR FC7, I HAVE NO CLUE WHY...GLIB MIGHT HAVE A PROBLEM AND MIGHT NEED TO BE SET TO TRUE
	const bool ignoreI2C = false;//FC7 ; true //GLIB???
	cSystemController.InitializeHw( cHWFile_ , std::cout);
	cSystemController.ConfigureHw ( ignoreI2C );
}

//========================================================================================================================
void FECBCInterface::halt (void)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	cSystemController.fBeBoardInterface->Stop( cSystemController.fBoardVector.at( 0 ));
}

//========================================================================================================================
void FECBCInterface::pause (void)
{
} 

//========================================================================================================================
void FECBCInterface::resume (void)
{
}

//========================================================================================================================
void FECBCInterface::start (std::string runNumber)
{
	__MOUT__ << "Starting run " << runNumber << " for Outer Tracker!" << std::endl;
	//std::string fileName = std::string(getenv("HOME")) + "/Data/Run_" + runNumber + ".raw";
	std::string fileName = dataFilePath_ + "/" + dataFileNamePrefix_ + runNumber;
	// if(cHWFile_.find("West") != std::string::npos)
	// 	fileName += "_West.raw";
	// else if(cHWFile_.find("East") != std::string::npos)
	// 	fileName += "_East.raw";
	// else
	fileName += ".raw";
	__MOUT__ << "Add file handler" << std::endl;
	if(saveDataFile_)
	{
	  cSystemController.addFileHandler( fileName, 'w' );
	  cSystemController.initializeFileHandler();
	}
//	std::string myFileName = "/data/TestBeam/2017_04_April/CMSOuterTracker/" + interfaceUID_ + "_Run" + runNumber + "_Raw.dat";
//	std::cout << __COUT_HDR_FL__ << "Saving file: " << myFileName << std::endl;
//	outFile_.open(myFileName.c_str(), std::ios::out | std::ios::binary);
//	if(!outFile_.is_open())
//	{
//		std::cout << __COUT_HDR_FL__ << "Can't open file " << fileName << std::endl;
//		assert(0);
//	}
	eventCounter_ = 0;
	// __MOUT__ << "SysController start" << std::endl;
	cSystemController.fBeBoardInterface->Start( cSystemController.fBoardVector.at( 0 ) );
	// __MOUT__ << "SysController done" << std::endl;
}

//========================================================================================================================
//The running state is a thread
bool FECBCInterface::running(void)
{
	// std::cout << __PRETTY_FUNCTION__ << "Begin!" << std::endl;
	
	if(cData_.size() != 0) cData_.clear();
	// Lorenzo ReadNData returns voind whileReadData returns fNpackets for the glib and cNEvents for the D19C...also fixed in the d19 the blocking while (cNWords == 0);
	// Lorenzo would be nice to have consistent fNpackets and cNEvents but we are not using any of the 2...
	uint32_t cPacketSize = cSystemController.ReadData ( cSystemController.fBoardVector.at( 0 ), cData_, false);

	if (cPacketSize!=0)
	{
	  // std::cout << __PRETTY_FUNCTION__ << "Packet Size: " << cPacketSize << std::endl;
//		int iic1 = 0;
//		for( unsigned int iic1=0;iic1<cData.size();iic1++)
//		{
//			outFile_.write( (char*)&cData.at(iic1), sizeof(uint32_t));
//			//std::cout<<iic1<<" "<< cData.at(iic1) <<std::endl;
//		}
		// std::cout << __PRETTY_FUNCTION__ << ">>>Consumer Recorded Events # " << cData_.size() << std::endl;
		// for(unsigned int i=0; i<cData_.size(); i++)
		//   std::cout << __PRETTY_FUNCTION__ << "# "<< i << " val: " << cData_[i] << std::endl;

		const std::vector<Event*>& events = cSystemController.GetEvents( cSystemController.fBoardVector.at( 0 ) );

		eventCounter_ += events.size();
		// if(eventCounter_%100 == 0)
		__MOUT__ << "Total recorded Events # " << std::hex << eventCounter_  << " -> " << std::dec << eventCounter_ << std::endl;
		// unsigned int count = 0;
		// for ( auto& ev : events )
		// {
		// 	std::cout << __PRETTY_FUNCTION__<< "Event #:" << ++count << std::endl;
		// 	std::cout << *ev << std::endl;
		// 	////	        std::cout << "COMPARING$$$$$$$$$$$$$$$$$$$$$$$$"<<std::endl;
		// 	////			//if ( count % 100  == 0 )
		// 	//			std::cout << ">>> Recorded Event #" << count << std::endl;
		// }

		// std::cout << __PRETTY_FUNCTION__ << "Streaming data..." << std::endl;
		// std::cout << __PRETTY_FUNCTION__ << "cData " << cData_.size() << " nEvents " << events.size() << " event " << events.size() << std::endl;
		TransmitterSocket::send(UDPDataStreamerBase::streamToSocket_, cData_);
		// std::cout << __PRETTY_FUNCTION__ << "Data streamed!" << std::endl;
	}
	else
		usleep(1000);

	return WorkLoop::continueWorkLoop_;//otherwise it stops!!!!!
}

//========================================================================================================================
void FECBCInterface::stop (void)
{
	cSystemController.fBeBoardInterface->Stop( cSystemController.fBoardVector.at( 0 ));
	__MOUT__ << "Total recorded Events # " << eventCounter_ << std::endl;

	//if(outFile_.is_open())
	//	outFile_.close();
}

DEFINE_OTS_INTERFACE(FECBCInterface)
