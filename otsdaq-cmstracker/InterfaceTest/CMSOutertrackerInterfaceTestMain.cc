#include "otsdaq/ConfigurationInterface/ConfigurationManager.h"
#include "otsdaq/ConfigurationInterface/ConfigurationInterface.h"
#include "otsdaq/TableCore/TableGroupKey.h"
#include <nlohmann/json.hpp>

#include "otsdaq-cmstracker/FEInterfaces/FEPowerSupplyInterface_interface.cc"

#include <iostream>
#include <fstream>

using namespace ots;

int main(int argc, char **argv)
{
	//Variables
	//const int          supervisorInstance_    = 1;
	const unsigned int configurationKeyValue_ = 28;

	ConfigurationManager* theConfigurationManager_ = new ConfigurationManager();

	std::string  XDAQContextTableName_         = "XDAQContextTable";
	std::string  supervisorContextUID_         = "PowerSupplyContext";
	std::string  supervisorApplicationUID_     = "PowerSupplyFESupervisor";
	std::string  interfaceUID_                 = "PowerSupplyFEInterface0";
	std::string  supervisorConfigurationPath_  = "/" + supervisorContextUID_ + "/LinkToApplicationTable/" + supervisorApplicationUID_ + "/LinkToSupervisorTable";
	const ConfigurationTree theXDAQContextConfigTree_ = theConfigurationManager_->getNode(XDAQContextTableName_);

	std::string configurationGroupName = "DefaultConfig";
	std::pair<std::string , TableGroupKey> theGroup(configurationGroupName, TableGroupKey(configurationKeyValue_));

	////////////////////////////////////////////////////////////////
	//INSERTED GLOBALLY IN THE CODE
	////////////////////////////////////////////////////////////////
	//
	//  ConfigurationManager*   theConfigurationManager_ = new ConfigurationManager;
	//  FEWInterfacesManager    theFEWInterfacesManager_(theConfigurationManager_, supervisorInstance_);
	//
	//  theConfigurationManager_->setupFEWSupervisorConfiguration(theConfigurationKey_,supervisorInstance_);
	//  theFEWInterfacesManager_.configure();
	//
	////////////////////////////////////////////////////////////////
	//Getting just the informations about the FEWInterface
	////////////////////////////////////////////////////////////////

	theConfigurationManager_->loadTableGroup(theGroup.first, theGroup.second, true);
	FEPowerSupplyInterface* theInterface_ = new FEPowerSupplyInterface(
			interfaceUID_,
			theXDAQContextConfigTree_,
			supervisorConfigurationPath_ + "/LinkToFEInterfaceTable/" + interfaceUID_ + "/LinkToFETypeTable");
	std::cout << "Done with new" << std::endl;

	// Test interface class methods here //
	theInterface_->configure();
	// std::cout << __PRETTY_FUNCTION__ << "Starting start procedure" << std::endl;
	// theInterface_->start(std::string(argv[1]));
	// unsigned int second = 1;//x1ms
	// unsigned int time = 10*second;
	// unsigned int counter=0;
	// while(counter++<time)
	// {
	// 	theInterface_->running(); //There is a 1s sleep inside the running
	// 	std::cout << counter << std::endl;
	// }
	// theInterface_->stop();

	return 0;
}
