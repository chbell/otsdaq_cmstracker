#ifndef _ots_OTDQMHistosConsumerConfiguration_h_
#define _ots_OTDQMHistosConsumerConfiguration_h_

#include "otsdaq/ConfigurationDataFormats/ConfigurationBase.h"

#include <string>


namespace ots
{

class OTDQMHistosConsumerConfiguration : public ConfigurationBase
{

public:

	OTDQMHistosConsumerConfiguration(void);
	virtual ~OTDQMHistosConsumerConfiguration(void);

	//Methods
	void init(ConfigurationManager *configManager);

	//Getter
	std::vector<std::string>  getProcessorIDList(void) const;
	std::string               getFilePath       (std::string processorUID) const;
	std::string               getRadixFileName  (std::string processorUID) const;
	bool                      getSaveFile       (std::string processorUID) const;

private:

	void check(std::string processorUID) const;
	enum{
		ProcessorID,
		FilePath,
		RadixFileName,
		SaveFile
	};

	std::map<std::string, unsigned int> processorIDToRowMap_;

};
}
#endif
