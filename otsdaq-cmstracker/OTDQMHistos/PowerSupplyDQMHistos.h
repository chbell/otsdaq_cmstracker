#ifndef _ots_PowerSupplyDQMHistos_h_
#define _ots_PowerSupplyDQMHistos_h_

#include "otsdaq/RootUtilities/DQMHistosBase.h"
#include "otsdaq-cmstracker/OTUtilities/PowerSupplyData.h"
#include <string>
#include <map>
#include <vector>

class TCanvas;
class TDirectory;
class TGraph;
class TMultiGraph;

namespace ots
{

class ConfigurationTree;
class PowerSupplyDQMHistos
{
public:
        PowerSupplyDQMHistos(bool deleteHistos=true);
        virtual ~PowerSupplyDQMHistos(void);
        
        void book           (TDirectory *myDirectory, const ConfigurationTree &theXDAQContextConfigTree, const std::string &theConfigurationPath); // again not sure if the configuration path should be brought here via this function or get it directly from plugin
        void destroy        (void);
        void clearPointers  (void);
        void fill           (std::string &buffer);
        void load           (std::string fileName){;}

        // variables used to get the data from the buffer
        std::map<std::string, std::string> configData;
        std::vector<float> currentVector;
        std::vector<float> voltageVector;
        std::vector<time_t> timeVector;

protected:
        TDirectory *myDir_;
        TDirectory *statusDir_;
 
        // canvas and graphs for the individual channels
        std::map<std::string, TCanvas *> canvasMap_;
        std::map<std::string, TGraph *> graphMap_;

        // maps to create multigraphs
        std::map<std::string, TGraph *> lowVoltageVoltageMap;
        std::map<std::string, TGraph *> lowVoltageCurrentMap;
        std::map<std::string, TGraph *> highVoltageVoltageMap;
        std::map<std::string, TGraph *> highVoltageCurrentMap;
                
        // canvas and multigraphs for histograms that combine data
        TCanvas     *cAllCurrents_;
        TCanvas     *cAllVoltages_;
        TMultiGraph *mgAllLVCurrents_;
        TMultiGraph *mgAllHVCurrents_;
        TMultiGraph *mgAllLVVoltages_;
        TMultiGraph *mgAllHVVoltages_;
private:
        PowerSupplyData thePowerSupplyData_;
        bool            deleteHistos_;
};

} // namespace ots

#endif
