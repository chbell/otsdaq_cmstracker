cet_make(LIBRARY_NAME OTDQMHistos
	    LIBRARIES
		OTUtilities
		ConfigurationInterface
	    #${ROOT_BASIC_LIB_LIST}
		${MF_MESSAGELOGGER}

)

install_headers()
install_source()
