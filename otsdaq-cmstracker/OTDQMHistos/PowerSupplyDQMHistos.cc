#include "otsdaq-cmstracker/OTDQMHistos/PowerSupplyDQMHistos.h"
#include "otsdaq/ConfigurationInterface/ConfigurationTree.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/MessageFacility/MessageFacility.h"

#include <iostream>
#include <sstream>
#include <string>

#include <TCanvas.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TFrame.h>
#include <TGraph.h>
#include <TH1.h>
#include <TH1F.h>
#include <TH2.h>
#include <TH2F.h>
#include <TMultiGraph.h>
#include <TProfile.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TThread.h>

using namespace ots;

#define DEFAULT_MARKER_STYLE 20
#define DEFAULT_MARKER_SIZE 0.3
//#define TIME_FORMAT "%H:%M:%S %m-%d-%Y"
#define TIME_FORMAT "%Y-%m-%d %H:%M:%S"

//========================================================================================================================
PowerSupplyDQMHistos::PowerSupplyDQMHistos(bool deleteHistos)
	: cAllCurrents_   (nullptr)
	, cAllVoltages_   (nullptr)
	, mgAllLVCurrents_(nullptr)
	, mgAllHVCurrents_(nullptr)
	, mgAllLVVoltages_(nullptr)
	, mgAllHVVoltages_(nullptr)
	, deleteHistos_   (deleteHistos)
{
	gStyle->SetOptTitle(0);
	gStyle->SetTitleFillColor(2);
	gStyle->SetTitleFontSize(0.2);
	gStyle->SetTimeOffset(0);
}

//========================================================================================================================
PowerSupplyDQMHistos::~PowerSupplyDQMHistos(void) 
{
	if(deleteHistos_)
		destroy();
}

//========================================================================================================================
void PowerSupplyDQMHistos::book(TDirectory *myDirectory, const ConfigurationTree &theXDAQContextConfigTree, const std::string &theConfigurationPath)
{
	//clearPointers();
	__MOUT__ << "Booking start!" << std::endl;
	myDir_ = myDirectory;
	statusDir_ = myDir_->mkdir("StatusPlot", "Status Plot"); //creates folder for these plots
	statusDir_->cd();

	for (auto &channel : theXDAQContextConfigTree.getNode(theConfigurationPath + "/LinkToPowerSupplyToDetectorTable").getChildren()) //creates as many canvases and graphs as necessary
	{
		std::string channelName = theXDAQContextConfigTree.getNode(theConfigurationPath + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/LinkToChannelUID").getValue();
		std::string channelType = theXDAQContextConfigTree.getNode(theConfigurationPath + "/LinkToPowerSupplyToDetectorTable/" + channel.second.getValueAsString() + "/ChannelType").getValue();

		TCanvas* aCanvas = nullptr;
		TGraph*  aGraph;
		aGraph = nullptr;
		if(canvasMap_.find(channelName) == canvasMap_.end() || (aCanvas = canvasMap_.find(channelName)->second) == nullptr)
		{
			aCanvas = canvasMap_[channelName] = new TCanvas(channelName.c_str(), channelName.c_str(), 800, 600);
			statusDir_->Add(aCanvas);
			aCanvas->Divide(1, 2);
			aCanvas->SetGridx();
			aCanvas->SetGridy();
		}
		//aCanvas here MUST point to an existing canvas. 
		//If aCanvas = nullptr it is created 
		//otherwise it is assigned in the second part of the or in the if statement

		if(graphMap_.find(channelName + "_Voltage") == graphMap_.end() || (aGraph = graphMap_.find(channelName)->second) == nullptr )
		{
			aGraph = graphMap_[channelName + "_Voltage"] = new TGraph();
			aGraph->SetNameTitle((channelName + "_Voltage").c_str(), (channelName + " Voltage").c_str());
			aGraph->SetMarkerColor(3);
			aGraph->SetMarkerStyle(DEFAULT_MARKER_STYLE);
			aGraph->SetMarkerSize(DEFAULT_MARKER_SIZE);
			aGraph->SetLineColor(kGreen);
			aGraph->GetXaxis()->SetTimeDisplay(1);
			aGraph->GetXaxis()->SetNdivisions(503);
			aGraph->GetXaxis()->SetTimeFormat(TIME_FORMAT);

			aCanvas->cd(1);
			aGraph->Draw("APL");
		}

		aGraph = nullptr;
		if(graphMap_.find(channelName + "_Current") == graphMap_.end() || (aGraph = graphMap_.find(channelName)->second) == nullptr )
		{
			aGraph = graphMap_[channelName + "_Current"] = new TGraph();
			aGraph->SetNameTitle((channelName + "_Current").c_str(), (channelName + " Current").c_str());
			aGraph->SetMarkerColor(3);
			aGraph->SetMarkerStyle(DEFAULT_MARKER_STYLE);
			aGraph->SetMarkerSize(DEFAULT_MARKER_SIZE);
			aGraph->SetLineColor(kGreen);
			aGraph->GetXaxis()->SetNdivisions(503);
			aGraph->GetXaxis()->SetTimeDisplay(1);
			aGraph->GetXaxis()->SetTimeFormat(TIME_FORMAT);

			aCanvas->cd(2);
			aGraph->Draw("APL");
		}

		if (channelType == "LV")
		{
			std::cout << "entered lv" << std::endl;
			lowVoltageVoltageMap[channelName + channelType] = graphMap_[channelName + "_Voltage"];
			lowVoltageCurrentMap[channelName + channelType] = graphMap_[channelName + "_Current"];
		}
		else if (channelType == "HV")
		{
			std::cout << "entered hv" << std::endl;
			highVoltageVoltageMap[channelName + channelType] = graphMap_[channelName + "_Voltage"];
			highVoltageCurrentMap[channelName + channelType] = graphMap_[channelName + "_Current"];
		}
		else
		{
			__SS__ << "ChannelType column for channel " << channelName << " must be configured to be LV or HV and cannot be DEFAULT" << __E__;
			__SS_THROW__;
		}
	}

	if (!cAllCurrents_)
	{
		cAllCurrents_ = new TCanvas("AllCurrents", "All Currents", 800, 600); //creation of multigraphs for currents and voltages
		statusDir_->Add(cAllCurrents_);
		cAllCurrents_->SetGridx();
		cAllCurrents_->SetGridy();
		cAllCurrents_->Divide(1, 2);

		mgAllLVCurrents_ = new TMultiGraph();
		mgAllLVCurrents_->SetNameTitle("allLVCurrents", "All Low Voltage Currents");
		mgAllLVCurrents_->GetXaxis()->SetTimeDisplay(1);
		mgAllLVCurrents_->GetXaxis()->SetNdivisions(503);
		mgAllLVCurrents_->GetXaxis()->SetTimeFormat(TIME_FORMAT);
		for (auto &listLowVoltageCurrents : lowVoltageCurrentMap)
		{
			mgAllLVCurrents_->Add(listLowVoltageCurrents.second);
		}	
		cAllCurrents_->cd(1);
		mgAllLVCurrents_->Draw("APL");

		mgAllHVCurrents_ = new TMultiGraph();
		mgAllHVCurrents_->SetNameTitle("allHVCurrents", "All High Voltage Currents");
		mgAllHVCurrents_->GetXaxis()->SetTimeDisplay(1);
		mgAllHVCurrents_->GetXaxis()->SetNdivisions(503);
		mgAllHVCurrents_->GetXaxis()->SetTimeFormat(TIME_FORMAT);
		for (auto &listHighVoltageCurrents : highVoltageCurrentMap)
		{
			mgAllHVCurrents_->Add(listHighVoltageCurrents.second);
		}
		cAllCurrents_->cd(2);
		mgAllHVCurrents_->Draw("APL");
	}

	if (!cAllVoltages_)
	{
		cAllVoltages_ = new TCanvas("AllVoltages", "All Voltages", 800, 600);
		statusDir_->Add(cAllVoltages_);
		cAllVoltages_->SetGridx();
		cAllVoltages_->SetGridy();
		cAllVoltages_->Divide(1, 2);

		mgAllLVVoltages_ = new TMultiGraph();
		mgAllLVVoltages_->SetNameTitle("allLVVoltages", "All LV Voltages");
		mgAllLVVoltages_->GetXaxis()->SetTimeDisplay(1);
		mgAllLVVoltages_->GetXaxis()->SetNdivisions(503);
		mgAllLVVoltages_->GetXaxis()->SetTimeFormat(TIME_FORMAT);
		for (auto &listLowVoltageVoltages : lowVoltageVoltageMap)
		{
			mgAllLVVoltages_->Add(listLowVoltageVoltages.second);
		}
		cAllVoltages_->cd(1);
		mgAllLVVoltages_->Draw("APL");

		mgAllHVVoltages_ = new TMultiGraph();
		mgAllHVVoltages_->SetNameTitle("allHVVoltages", "All HV Voltages");
		mgAllHVVoltages_->GetXaxis()->SetTimeDisplay(1);
		mgAllHVVoltages_->GetXaxis()->SetNdivisions(503);
		mgAllHVVoltages_->GetXaxis()->SetTimeFormat(TIME_FORMAT);
		for (auto &listHighVoltageVoltages : highVoltageVoltageMap)
		{
			mgAllHVVoltages_->Add(listHighVoltageVoltages.second);
		}
		cAllVoltages_->cd(2);
		mgAllHVVoltages_->Draw("APL");
	}

	__MOUT__ << "Booking done!" << std::endl;
}

//========================================================================================================================
void PowerSupplyDQMHistos::fill(std::string &buffer)
{
	__MOUT__ << "Filling histos!" << std::endl;
	try
	{ 
		//get the information from buffer and convert it from Json format
		auto jsonMap = thePowerSupplyData_.convertFromJson(buffer);
		std::map<std::string, PowerSupplyData::ChannelStatus> channelInfoMap = jsonMap["PowerSupplyData"];
		std::string channelName;
		for (auto &channel : channelInfoMap)
		{ //fill graphs with values from buffer
			channelName = channel.first;
			std::map<std::string, TGraph *>::iterator aGraph;
			if ((aGraph = graphMap_.find(channelName + "_Voltage")) != graphMap_.end())
			{
				__MOUT__ << "Channel: " << channelName << " Time: " << channel.second.time << " V: " << channel.second.voltage << " C: " << channel.second.current << std::endl;
				aGraph->second->SetPoint(aGraph->second->GetN(), channel.second.time, channel.second.voltage);
			}
			if ((aGraph = graphMap_.find(channelName + "_Current")) != graphMap_.end())
			{
				__MOUT__ << "Channel: " << channelName << " Time: " << channel.second.time << " V: " << channel.second.voltage << " C: " << channel.second.current << std::endl;
				aGraph->second->SetPoint(aGraph->second->GetN(), channel.second.time, channel.second.current);
			}
		}
		// cAllCurrents_->cd(1); // fill multigraphs
		// mgAllHVCurrents_->Draw("APL");
		// mgAllHVCurrents_->GetHistogram()->GetXaxis()->SetLimits(mgAllHVCurrents_->GetHistogram()->GetXaxis()->GetBinLowEdge(1), channelInfoMap[channelName].time + 1);

		// cAllCurrents_->cd(2);
		// mgAllLVCurrents_->Draw("APL");
		// mgAllLVCurrents_->GetHistogram()->GetXaxis()->SetLimits(mgAllLVCurrents_->GetHistogram()->GetXaxis()->GetBinLowEdge(1), channelInfoMap[channelName].time + 1);

		// cAllVoltages_->cd(1);
		// mgAllHVVoltages_->Draw("APL");
		// mgAllHVVoltages_->GetHistogram()->GetXaxis()->SetLimits(mgAllHVVoltages_->GetHistogram()->GetXaxis()->GetBinLowEdge(1), channelInfoMap[channelName].time + 1);

		// cAllVoltages_->cd(2);
		// mgAllLVVoltages_->Draw("APL");
		// mgAllLVVoltages_->GetHistogram()->GetXaxis()->SetLimits(mgAllLVVoltages_->GetHistogram()->GetXaxis()->GetBinLowEdge(1), channelInfoMap[channelName].time + 1);
	}
	catch (const std::runtime_error &e)
	{
		//The status was probably not ready so just returning...
		__MOUT__ << "Exception filling histos!" << std::endl;
		return;
	}
}

//========================================================================================================================
void PowerSupplyDQMHistos::destroy()
{
	for(auto cIt=canvasMap_.begin(); cIt!=canvasMap_.end(); cIt++)
	{
		delete cIt->second;
	}
	canvasMap_.clear();
	for(auto gIt=graphMap_.begin(); gIt!=graphMap_.end(); gIt++)
	{
		delete gIt->second;
	}
	graphMap_.clear();

	if(cAllCurrents_ == nullptr)
		delete cAllCurrents_;
	if(cAllVoltages_ == nullptr)
		delete cAllVoltages_;
	if(mgAllLVCurrents_ == nullptr)
		delete mgAllLVCurrents_;
	if(mgAllHVCurrents_ == nullptr)
		delete mgAllHVCurrents_;
	if(mgAllLVVoltages_ == nullptr)
		delete mgAllLVVoltages_;
	if(mgAllHVVoltages_ == nullptr)
		delete mgAllHVVoltages_;

	clearPointers();
}

//========================================================================================================================
void PowerSupplyDQMHistos::clearPointers()
{
	canvasMap_.clear();//Once the map is cleared the pointers are gone so no need to set them to null
	graphMap_.clear();//Once the map is cleared the pointers are gone so no need to set them to null
	cAllCurrents_    = nullptr;
	cAllVoltages_    = nullptr;
	mgAllLVCurrents_ = nullptr;
	mgAllHVCurrents_ = nullptr;
	mgAllLVVoltages_ = nullptr;
	mgAllHVVoltages_ = nullptr;
}
