//GET NEW VERSION OF UHAL
You need to change: 
1) the branch in .gitmodules
2) in the CMakeLists.txt (the one in the otsdaq_cmstracker dir) the version un uhal

//COMPILE uhal in OTSDAQ
// uhal can now be compiled with the cmake that is in this directory just downloading the ipbus-software
//https://ipbus.web.cern.ch/doc/user/html/software/install/compile.html
//In case you just want to compile it in otsdaq without the cmake, then use the following command to link it to the right boost libraries

make -j8 EXTERN_BOOST_INCLUDE_PREFIX=${BOOST_INC} EXTERN_BOOST_LIB_PREFIX=${BOOST_LIB}
make install prefix=/home/modtest/Programming/otsdaq_develop_git/srcs/otsdaq_cmstracker/uhal/uhal_compiled

