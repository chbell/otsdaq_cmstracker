# Otsdaq CMS Tracker

## OTSDAQ Core Installation
This code is an extension of the otsdaq project: https://github.com/art-daq/otsdaq/wiki project and requires it to function. 
The following instruction must be executed once and they are needed to install OTSDAQ.
Once the OTSDAQ core dependencies are set up, the CMS Tracker interface code can be included.  

##  Install OTSDAQ system dependencies (need to be root or have sudo powers):
<pre>
sudo yum install -y git libuuid-devel openssl-devel curl-devel #as root for basic tools
sudo yum install -y gcc kernel-devel make #as root for compiling
sudo yum install -y lsb #as root for lsb_release
sudo yum install -y kde-baseapps #as root for kdialog
sudo yum install -y python2-devel elfutils-devel 
sudo yum install -y epel-release #repository to find libzstd and xxhash
sudo yum install -y libzstd xxhash xxhash-devel
sudo yum install -y mesa-lib*
sudo yum install -y pugixml pugixml-devel
</pre>

## Set up your products area:
<pre>
sudo yum install -y https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
sudo yum clean all
sudo yum install -y cvmfs cvmfs-config-default
</pre>

create the file /etc/cvmfs/default.d/70-artdaq.conf and add the following lines in the 

<pre>
CVMFS_REPOSITORIES=fermilab.opensciencegrid.org
CVMFS_HTTP_PROXY=DIRECT
</pre>

Then you can refresh the cvmfs configuration:

<pre>
sudo cvmfs_config setup
# Check if CernVM-FS mounts the specified repositories by: 
sudo cvmfs_config probe
</pre>
If the probe succed you should see this line

`Probing /cvmfs/fermilab.opensciencegrid.org... OK`

If the probe fails, try to restart autofs with 

`sudo service autofs restart`

If this one fails too, then check the documentation of the CERN cvmfs system here:

https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html

**From now on you don't need sudo powers anymore and it is reccommended to install the software as a regular user**


## Install OTSDAQ in user area 

If cvmfs is installed correctly, now you can export the PRODUCTS directory:
<pre>
export PRODUCTS=/cvmfs/fermilab.opensciencegrid.org/products/artdaq/
</pre>

## Fetch code from the repository

<pre>
cd $HOME
mkdir otsdaq
cd otsdaq
export OTSDAQ_HOME=$PWD
bash
source $PRODUCTS/setup # e.g. /data/ups/setup, Products set in previous step
Base=$PWD
setup git
setup gitflow
setup mrb
export MRB_PROJECT=otsdaq
mrb newDev -f -q s118:e26:prof

. $Base/local*/setup
cd $MRB_SOURCE
</pre>

Get the main software repositories
<pre>
mrb gitCheckout -b bb47b9deb https://github.com/art-daq/otsdaq.git
mrb gitCheckout -b b114106f -d otsdaq_utilities https://github.com/art-daq/otsdaq_utilities.git
</pre>

OTSDAQ is now installed but **NOT** compiled!
You should continue with the installation of the Tracker package and the BurninBox package (https://gitlab.cern.ch/otsdaq/otsdaq_cmsburninbox) if needed.
You can compile anytime but do it after installing all packages saves you a bit of time.

## Installation of the Tracker package
<pre>
cd $MRB_SOURCE # this is the 'srcs' directory that will be set in the course of setting up OTSDAQ
git clone -b develop --recurse-submodules https://gitlab.cern.ch/otsdaq/otsdaq_cmstracker.git otsdaq_cmstracker
mrb uc
</pre>

## Installation of the BurninBox package (If you are installing otsdaq for the BurninBox)
If you are installing otsdaq for the BurninBox system you need to install also the BurninBox package

<pre>
cd $MRB_SOURCE # this is the 'srcs' directory that will be set in the course of setting up OTSDAQ
git clone https://gitlab.cern.ch/otsdaq/otsdaq_cmsburninbox.git otsdaq_cmsburninbox
mrb uc 
</pre>

There is an extra step for the BurninBox GUI that needs to point to a specific area

<pre>
cd $OTSDAQ_HOME
ln -fs $MRB_SOURCE/otsdaq_cmsburninbox/UserWebGUI $MRB_SOURCE/otsdaq_utilities/WebGUI/CMSBurninBoxWebPath
</pre>


## Copy the setup file 
**If you are installing the software for the BurninBox**

<pre>
cd $OTSDAQ_HOME
cp $MRB_SOURCE/otsdaq_cmsburninbox/SetupFiles/BurninBoxSetup.sh .
source BurninBoxSetup.sh
</pre>

**If instead you ONLY installed the tracker package, then copy the setup file for the tracker, otherwise skip this step and USE the BurninBoxSetup.sh**
<pre>
cd $OTSDAQ_HOME
cp $MRB_SOURCE/otsdaq_cmstracker/SetupFiles/TrackerSetup.sh .
source TrackerSetup.sh
</pre>

Once the package is checked out **or every time that you start a new session**, source the BurninBoxSetup.sh (or TrackerSetup.sh).

**N.B. When sourcing the BurninBoxSetup.sh (or TrackerSetup.sh) file, WARNINGS are not a problem and can be ignored.**

##  Compile
<pre>
cd $OTSDAQ_HOME
source BurninBoxSetup.sh # (or source TrackerSetup.sh)
</pre>

Right now protobuf must be compiled manually with the following commands, after the correct g++ is sourced (source BurninBoxSetup.sh or TrackerSetup.sh)
This needs to be done **only the first time** or if protobuf is upgraded.

<pre>
cd $MRB_SOURCE
mrb uc
cd $MRB_SOURCE/otsdaq_cmstracker/protobuf/protobuf_3_19_4
./autogen.sh
./configure --prefix=$MRB_SOURCE/otsdaq_cmstracker/protobuf
make -j$(nproc) # $(nproc) ensures it uses all cores for compilation
make install
</pre>

To compile otsdaq we are using mrb and some aliases are defined in the TrackerSetup.sh 
<pre>
cd $OTSDAQ_HOME
mz #if it is the first time or when the project need to to be cleaned (use mb instead if you already started a compilation once with mz)
</pre>

**If there are no errors while compiling you have installed OTSDAQ!**

##  READ THIS FIRST BEFORE RUNNING OTSDAQ
**The Setup file you just copied (BurninBoxSetup.sh or TrackerSetup.sh) is a generic setup file, which is very similar to the Setup file example in the next paragraph.**


**There are 5 important variables that needs to be modified in order for OTSDAQ to work in YOUR environment**
1) USER_DATA
2) ARTDAQ_DATABASE_URI
3) OTSDAQ_DATA
4) BURNINBOX_CONFIGURATION_FILE
5) MODULE_NAMES_FILE

Variables meaning:
1) USER_DATA  -> points to a directory where Logs, RunNumber and other infos are stored. Each center will have its own USER_DATA directory.
2) ARTDAQ_DATABASE_URI -> has the database with the history of all configurations
3) OTSDAQ_DATA -> is the directory where **ALL DATA WILL BE SAVED**
4) BURNINBOX_CONFIGURATION_FILE -> if you use the BurninBox, you MUST configure this file according to the way YOU wired your box
5) MODULE_NAMES_FILE -> is the file where the module names are stored when a burnin cycle starts

**My reccomendation is to copy the default configurations to your custom configurations and then modify them accordingly**

**EXAMPLE**
I like to use my institution name to store those quantities so in the following example I will use Fermilab. For the other centers try to use any of the following please:
Brussels
DESY
Louvain
NCP
Niser
Pisa
Princeton
Rutgers

Create a directory where you want to store the data, **for example: $HOME/CMSBurninBox** and substitute FErmilab with your own institution name

<pre>
mkdir $HOME/CMSBurninBox
cd $HOME/CMSBurninBox
cp -rfp ${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/DataDefault $HOME/CMSBurninBox/DataFermilab
cp -rfp ${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/databases/filesystemdb/2023_03_Default_db $HOME/CMSBurninBox/filesystemdb/2023_03_Fermilab_db
cp -p ${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/BurninBox/configuration/BurninBoxConfiguration_Default.xml ${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/BurninBox/configuration/BurninBoxConfiguration_Fermilab.xml
</pre>


##  Running OTSDAQ the VERY FIRST TIME
From a terminal window
<pre>
cd $OTSDAQ_HOME/otsdaq
source BurninBoxSetup.sh # (or source TrackerSetup.sh)
ots -w
</pre>

The **ots -w** command transforms some environmental variables into the link they correspond to. This command must be executed ONLY the very first time after a clean installation of OTSDAQ. 

##  Running OTSDAQ
From a terminal window
<pre>
cd $OTSDAQ_HOME/otsdaq
source BurninBoxSetup.sh # (or source TrackerSetup.sh)
ots
</pre>

Copy and paste in Firefox or Chrome the link that appears on the terminal when OTSDAQ has started.




##  Setup file example
```sh
echo # This script is intended to be sourced.

sh -c "[ `ps $$ | grep bash | wc -l` -gt 0 ] || { echo 'Please switch to the bash shell before running the otsdaq-demo.'; exit; }" || exit

echo -e "setup [275]  \t ======================================================"
echo -e "setup [275]  \t Initially your products path was PRODUCTS=${PRODUCTS}"

unsetup_all

export OTSDAQ_HOME=$PWD
#export PRODUCTS=$OTSDAQ_HOME/products
export PRODUCTS=/cvmfs/fermilab.opensciencegrid.org/products/artdaq/

source ${PRODUCTS}/setup

setup mrb
setup git
source ${OTSDAQ_HOME}/localProducts_otsdaq_*/setup

#source mrbSetEnv
mrbsetenv
echo -e "setup [275]  \t Now your products path is PRODUCTS=${PRODUCTS}"
echo

# Setup environment when building with MRB (As there's no setupARTDAQOTS file)

export CETPKG_INSTALL=${PRODUCTS}
export CETPKG_J=`nproc`

export OTS_MAIN_PORT=2015

export USER_DATA="${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/DataDefault"
export ARTDAQ_DATABASE_URI="filesystemdb://${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/databases/filesystemdb/2023_03_Default_db"
export OTSDAQ_DATA="/data/CMSBurninBox"

##### BURNIN BOX CONFIGURATION ##########
export BURNINBOX_CONFIGURATION_FILE="${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/BurninBox/configuration/BurninBoxConfiguration_Default.xml"
export OTSDAQ_CMSBURNINBOX_LIB=`echo ${MRB_BUILDDIR}/otsdaq_cmsburninbox/slf7*/lib`
export MODULE_NAMES_FILE="/tmp/ModuleNames.cfg"
##### OUTERTRACKER CONFIGURATION ##########
export PH2ACF_BASE_DIR=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/Ph2_ACF
export ACFSUPERVISOR_ROOT=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/ACFSupervisor
export Protobuf_DIRS=${OTSDAQ_CMSTRACKER_DIR}/protobuf

#########
# Flags #
#########
export HttpFlag='-D__HTTP__'
export ZmqFlag='-D__ZMQ__'
export USBINSTFlag='-D__USBINST__'
export Amc13Flag='-D__AMC13__'
export AntennaFlag='-D__ANTENNA__'
export UseRootFlag='-D__USE_ROOT__'
export MultiplexingFlag='-D__MULTIPLEXING__'
export EuDaqFlag='-D__EUDAQ__'
################
# Compilations #
################

# Stand-alone application, without data streaming
#        export CompileForHerd=false
#        export CompileForShep=false

# Stand-alone application, with data streaming
export CompileForHerd=true
export CompileForShep=true

# Herd application
# export CompileForHerd=true
# export CompileForShep=false

# Shep application
# export CompileForHerd=false
# export CompileForShep=true

# Compile with EUDAQ libraries
export CompileWithEUDAQ=false

echo -e "setup [275]  \t Now your user data path is USER_DATA \t\t = ${USER_DATA}"
echo -e "setup [275]  \t Now your database path is ARTDAQ_DATABASE_URI \t = ${ARTDAQ_DATABASE_URI}"
echo -e "setup [275]  \t Now your output data path is OTSDAQ_DATA \t = ${OTSDAQ_DATA}"
echo

alias rawEventDump="art -c /home/modtest/Programming/otsdaq/srcs/otsdaq/artdaq-ots/ArtModules/fcl/rawEventDump.fcl"
alias kx='StartOTS.sh -k'

echo
echo -e "setup [275]  \t Now use 'StartOTS.sh --wiz' to configure otsdaq"
echo -e "setup [275]  \t  	Then use 'StartOTS.sh' to start otsdaq"
echo -e "setup [275]  \t  	Or use 'StartOTS.sh --help' for more options"
echo
echo -e "setup [275]  \t     use 'kx' to kill otsdaq processes"
echo

#setup ninja v1_8_2
#setup nlohmann_json v3_9_0b -q e19:prof


#setup ninja generator
#============================
ninjaver=`ups list -aKVERSION ninja|sort -V|tail -1|sed 's|"||g'`
setup ninja $ninjaver
alias mb='pushd $MRB_BUILDDIR; ninja -j$CETPKG_J; popd'
alias mbb='mrb b --generator ninja'
alias mz='mrb z; mrbsetenv; mrb b --generator ninja'
alias mt='pushd $MRB_BUILDDIR;ninja -j$CETPKG_J;CTEST_PARALLEL_LEVEL=${CETPKG_J} ninja -j$CETPKG_J test;popd'
alias mtt='mrb t --generator ninja'
alias mi='pushd $MRB_BUILDDIR; ninja -j$CETPKG_J install;popd'
alias mii='mrb i --generator ninja'
```



